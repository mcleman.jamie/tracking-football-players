
Each file in this folder shows different outputs that the program produces. Following is an explanation of what each represents:

detectionsCamera(1-6):
These videos show the original video footage of each camera labelled 1-6 with the detection and classification of each player shown.
The detection is represented by a coloured rectangle where the colour of the rectangle represents the team classification.

camerasOutput:
This video shows an aerial view of the pitch with many circles. These circles represent all the player detections from all the cameras.
The colour of the circle represents the camera which it was detected by. As the players move from one side of the pitch to the other,
you will see how the colours change as they are now in the field of view of different cameras. You will also notice that there 
are usually different coloured circles moving in pairs. This is because there are cameras on each side of the pitch and so usually 
they are detected by two opposite cameras simultaneously.

classificationsOutput:
This video shows an aerial view of the pitch with many circles. These circles represent all the player detections from all the cameras. 
The colour of the circle represents the team classification of each detection. The team classifications are blue, white and red where 
blue and white are each team (one team wears blue and the other wears white), and red is the referees.

mainOutput:
This is the main output of the program. It shows an aerial view of the pitch with many circles where each circle represents a player or referee. 
This differs from the previous two outputs in that duplicate detections of the same player have been assimilated. There is also a black line that
is attached to each circle. This represents the path of the player over the the previous second. There are two new classifications of the 
goalkeepers as they wear different coloured shirts, yellow and black.