
Tracking Football Players by Jamie McLeman.

This project was completed between 2020 and 2021 as part of my 
final year project in my third year of university at University 
of Manchester. For more information on the project please view 
my report located at:

Report and Output/Tracking_Football_Players_Report.pdf

Thank you.

