#ifndef PLAYER_H
#define PLAYER_H
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include "TeamClassifier.hpp"

using namespace cv;

class Detection {
public:
	Detection(Point2f TopLeftPos, Point2f CentrePos, Point2f Size, int CameraDetectedWith);
	Rect boundingBox;
	Point2f topLeftPos;
	Point2f centrePos;
	Point2f size;
	int cameraDetectedWith;
	std::vector<int> associatedTracklets;
	const std::vector<int>& getAssociatedTracklets() { return associatedTracklets; }

};

class TrackedDetection {
public:
	TrackedDetection(Rect BoundingBox, int CameraDetectedWith);
	void predict();
	void correct(Rect CorrectedBoundingBox, bool Occluded);
public:
	Rect boundingBox;
	Rect predictedBoundingBox;
	Point2f centrePos;
	Rect boundingBoxVelocity;

	std::vector<float> teamPercentages;
	Point2f pitchPosition;
	int teamID;
	int cameraDetectedWith;
	bool occluded;
};

class TrackedPlayer {
public:
	TrackedPlayer();

	static int nextID;
	const static int smoothingKernelSize = 11;
	int ID;

	Point2f position;
	Point2f predictedPosition;
	Point2f velocity;
	int teamID;
	std::vector<float> teamPercentages;
	std::vector<int> teamClassificationFrequency;
	std::vector<Point2f> positionHistory;
	std::vector<int> teamHistory;
	int previousSmoothedTeamValue;
	int updatesWithoutDetection;
	int lifetime;
	bool alive;
	bool teamSwitched;
	int switchedToID;
	int firstFrameDetected;
	bool possibleSwitch;
	int switchCountdown;
public:
	void init(Point2f Position, std::vector<float> TeamPercentages, int FirstFrameDetected);
	void predict();
	void correctPosition(Point2f CorrectedPosition);
	void correctTeam(std::vector<float>& TeamPercentages);
	void correctUndetected();
	void switchToNewTracklet(TrackedPlayer& newTracklet, int CurrentFrame);
	void mergeTracklet(TrackedPlayer& TrackletToMerge);
};

class SavedPlayer {
public:
	SavedPlayer() { return; }
	int firstFrameDetected;
	int lifetime;
	int teamID;
	std::vector<Point2f> positionHistory;
};

#endif