#ifndef PLAYER_DETECTION_H
#define PLAYER_DETECTION_H
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include "Player.hpp"

using namespace cv;

class PlayerDetection {
public:
	PlayerDetection() { return; }
	void resetDetections();
	void getNewDetections(const Mat& Foreground, int CameraID);
	void update(std::vector<TrackedDetection> &trackedDetections);
	void validateTrackedDetections(std::vector<TrackedDetection>& trackedDetections, int MaxX, int MaxY);
public:
	std::vector<Detection> detections;
};

#endif