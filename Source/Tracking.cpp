#include "Tracking.hpp"

Tracking::Tracking() {

	// Initialise some variables
	startedTracking = false;
	associationMatrix = Mat::zeros(1, 1, CV_32F);
	distanceAssociationThreshold = 2.5;

}

void Tracking::update(int CurrentFrame, std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	currentFrame = CurrentFrame;

	// If we haven't started tracking yet
	if (!startedTracking) {
		initialiseTracklets(Tracklets, Detections);
		return;
	}

	// Predict next positions
	for (int i = 0; i < Tracklets.size(); i++) {
		Tracklets[i].predict();
	}

	bool associatingFinished = false;
	// Loop until constraints are passed
	while (!associatingFinished) {

		// Associate
		associateDetectionsToTracklets(Tracklets, Detections);

		// Apply constraints
		associatingFinished = applyConstraints(Tracklets, Detections);
	}

	// Combine detections
	combineDetections(Tracklets, Detections);
}

void Tracking::initialiseTracklets(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	// If there aren't any detections, we cannot initialise any tracklets
	if (Detections.size() == 0) {
		return;
	}
	
	bool associatingFinished = false;
	// Loop until constraints are passed
	while (!associatingFinished) {
		
		// Associate
		associateDetectionsToTracklets(Tracklets, Detections);

		// Apply constraints
		associatingFinished = applyConstraints(Tracklets, Detections);
	}

	// Combine detections
	combineDetections(Tracklets, Detections);

	// We have now started tracking
	startedTracking = true;
}


void Tracking::associateDetectionsToTracklets(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	bool associated;
	int firstIndex = 0;

	// If very first detection
	if (Tracklets.size() == 0) {

		// Create an association matrix of size with space for 1 tracklet
		associationMatrix = Mat::zeros(Detections.size(), 1, CV_32F);
		associationMatrix.at<float>(0, 0) = 1;

		// Create the first tracklet from the first detections
		Tracklets.push_back(TrackedPlayer());
		Tracklets.back().init(Detections[0].pitchPosition, Detections[0].teamPercentages, currentFrame);
		firstIndex = 1;
	}
	else {
		// Initialse empty association matrix of size (numDetections, numTracklets)
		associationMatrix = Mat::zeros(Detections.size(), Tracklets.size(), CV_32F);
	}

	// Iterate through all the detections
	for (int i = firstIndex; i < Detections.size(); i++) {

		associated = false;
		// Iterate through all the tracklets
		for (int j = 0; j < Tracklets.size(); j++) {

			// If it's dead, ignore it
			if (!Tracklets[j].alive) {
				continue;
			}
			
			// Get the distance
			float distance = sqrt(pow(Detections[i].pitchPosition.x - Tracklets[j].predictedPosition.x, 2) + pow(Detections[i].pitchPosition.y - Tracklets[j].predictedPosition.y, 2));

			// If the distance is less than the threshold, associate
			if (distance < distanceAssociationThreshold) {

				associationMatrix.at<float>(i, j) = 1;
				associated = true;
			}
		}

		// If the current detection is not associated and we haven't exceded max tracklets limit
		if (!associated && Tracklets.size() < 25) {

			// Create a new tracklet for this detection
			addColumnToMat(associationMatrix);
			associationMatrix.at<float>(i, Tracklets.size()) = 1;
			Tracklets.push_back(TrackedPlayer());
			Tracklets.back().init(Detections[i].pitchPosition, Detections[i].teamPercentages, currentFrame);
		}
	}
}

void Tracking::combineDetections(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	Point2f newMeasurementPosition;
	std::vector<float> averageTeamPercentages;
	for (int i = 0; i < 5; i++) {
		averageTeamPercentages.push_back(0);
	}

	// Find the tracklets furthest to each side of the pitch
	int furthestLeftID = 0;
	int furthestRightID = 0;
	int furthestTopID = 0;
	int furthestBottomID = 0;
	findExtremeTracklets(Tracklets, furthestLeftID, furthestRightID, furthestTopID, furthestBottomID);

	for (int i = 0; i < Tracklets.size(); i++) {

		if (getAveragesOfDetections(i, Detections, newMeasurementPosition, averageTeamPercentages)) {

			// Left goalkeeper
			if (i == furthestLeftID) {
				averageTeamPercentages = { 0, 0, 0, 0, 1.0 };
			}
			// Right goalkeeper
			else if (i == furthestRightID) {
				averageTeamPercentages = { 0, 0, 0, 1.0, 0 };
			}
			// Linesmen
			else if (i == furthestTopID || i == furthestBottomID) {
				averageTeamPercentages = { 0, 0, 1.0, 0, 0 };
			}

			// If already tracking, correct the position/team
			if (startedTracking) {
				Tracklets[i].correctPosition(newMeasurementPosition);
				Tracklets[i].correctTeam(averageTeamPercentages);
			}

			// Otherwise, reinitialise the tracklets with
			else {
				Tracklets[i].init(newMeasurementPosition, averageTeamPercentages, currentFrame);
			}
		}
		// If undetected
		else {
			if (Tracklets[i].alive) {
				Tracklets[i].correctUndetected();
			}
		}
	}
}

bool Tracking::getAveragesOfDetections(int TrackletID, const std::vector<TrackedDetection> &Detections, Point2f& AveragePosition, std::vector<float>& AverageTeamPercentages) {
	
	for (int i = 0; i < AverageTeamPercentages.size(); i++) {
		AverageTeamPercentages[i] = 0;
	}

	AveragePosition = Point2f(0, 0);
	int numPositionMeasurements = 0;
	int numTeamMeasurements = 0;

	// Iterate through all detections in the association matrix
	for (int j = 0; j < associationMatrix.rows; j++) {

		// If this detection is associated to the tracklet
		if (associationMatrix.at<float>(j, TrackletID) > 0) {

			// Accumulate position
			AveragePosition += Detections[j].pitchPosition;
			numPositionMeasurements++;
			
			// Accumulate team percentages if it's not occluded
			if (!Detections[j].occluded) {
				AverageTeamPercentages[0] += Detections[j].teamPercentages[0];
				AverageTeamPercentages[1] += Detections[j].teamPercentages[1];
				AverageTeamPercentages[2] += Detections[j].teamPercentages[2];
				numTeamMeasurements++;
			}
		}
	}

	// If there are no measurements, then the tracklet is undetected
	if (numPositionMeasurements == 0) {
		return false;
	}

	// Find the average position
	AveragePosition /= numPositionMeasurements;

	// If it's fully occluded, set all to -1
	if (numTeamMeasurements == 0) {
		AverageTeamPercentages[0] = -1;
		AverageTeamPercentages[1] = -1;
		AverageTeamPercentages[2] = -1;
		AverageTeamPercentages[3] = -1;
		AverageTeamPercentages[4] = -1;
	}

	// Otherwise, find the average team percentages 
	else {
		AverageTeamPercentages[0] /= numPositionMeasurements;
		AverageTeamPercentages[1] /= numPositionMeasurements;
		AverageTeamPercentages[2] /= numPositionMeasurements;
	}

	return true;
}

void Tracking::findExtremeTracklets(const std::vector<TrackedPlayer> Tracklets, int& Left, int& Right, int& Top, int& Bottom) {

	// Initialise extreme values
	int furthestLeftX = Tracklets[0].position.x;
	int furthestRightX = Tracklets[0].position.x;
	int furthestTopY = Tracklets[0].position.y;
	int furthestBottomY = Tracklets[0].position.y;
	Left = 0;
	Right = 0;
	Top = 0;
	Bottom = 0;

	// Iterate through all the tracklets
	for (int i = 1; i < Tracklets.size(); i++) {
		// Left
		if (Tracklets[i].position.x < furthestLeftX) {
			furthestLeftX = Tracklets[i].position.x;
			Left = i;
		}
		// Right
		else if (Tracklets[i].position.x > furthestRightX) {
			furthestRightX = Tracklets[i].position.x;
			Right = i;
		}
		// Top
		if (Tracklets[i].position.y < furthestTopY) {
			furthestTopY = Tracklets[i].position.y;
			Top = i;
		}
		// Bottom
		else if (Tracklets[i].position.y > furthestBottomY) {
			furthestBottomY = Tracklets[i].position.y;
			Bottom = i;
		}
	}

	// If the top and bottom values are not outside the region of the pitch
	// they are not linesmen
	if (furthestTopY > 0) {
		Top = -1;
	}
	if (furthestBottomY < 74) {
		Bottom = -1;
	}
}


bool Tracking::applyConstraints(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	// Apply constraints
	if (!mergeConstraint(Tracklets, Detections)) {
		return false; // Return false so we can reassociate
	}
	sameCameraConstraint(Tracklets, Detections);
	singleDetectionConstraint(Tracklets, Detections);


	// Check if there are detections that have no associations
	// Create new tracklets if there are and return false so we can reassociate
	if (Tracklets.size() < 25) {

		bool unassociated;
		for (int i = 0; i < associationMatrix.rows; i++) {

			unassociated = true;
			for (int j = 0; j < associationMatrix.cols; j++) {

				if (associationMatrix.at<float>(i, j) > 0) {
					unassociated = false;
					break;
				}
			}

			// If no associations found for detection i
			if (unassociated) {

				// Create new tracklet
				addColumnToMat(associationMatrix);
				associationMatrix.at<float>(i, Tracklets.size()) = 2;
				Tracklets.push_back(TrackedPlayer());
				Tracklets.back().init(Detections[i].pitchPosition, Detections[i].teamPercentages, currentFrame);
				return false;
			}
		}
	}
	return true;
}

bool Tracking::mergeConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	std::vector<bool> duplicates;
	for (int i = 0; i < associationMatrix.cols; i++) {
		duplicates.push_back(false);
	}
	std::vector<std::vector<int>> duplicateTracklets;
	std::vector<std::vector<int>> duplicateDetections;
	std::vector<int> tracklets;
	std::vector<int> detections;
	bool duplicate;
	bool empty;

	// Iterate through all tracklets
	for (int i = 0; i < associationMatrix.cols - 1; i++) {

		// If this tracklet has been flagged as a duplicate from before
		if (duplicates[i] == true) {
			continue;
		}

		// Reset
		duplicate = false;
		empty = true;
		detections.clear();
		tracklets.clear();
		tracklets.push_back(i);

		// Find all detections for this tracklet
		for (int j = 0; j < associationMatrix.rows; j++) {
			if (associationMatrix.at<float>(j, i) > 0) {
				detections.push_back(j);
				empty = false;
			}
		}
		// Ignore a tracklet with no detections
		if (empty) {
			continue;
		}

		// Compare current tracklet to all subsequent tracklets
		for (int j = i + 1; j < associationMatrix.cols; j++) {
			if (duplicates[j] == true) {
				continue;
			}
			if (compareMats(associationMatrix.col(i), associationMatrix.col(j))) {
				duplicates[j] = true;
				tracklets.push_back(j);
				duplicate = true;
			}
		}

		// If this tracklet has been flagged as a duplicate
		if (duplicate) {

			duplicates[i] = true;
			duplicateTracklets.push_back(tracklets);
			duplicateDetections.push_back(detections);
		}
	}

	// Vector that stores the number of associations for each camera
	std::vector<int> cameraAssociationCount;
	for (int i = 0; i < 6; i++) {
		cameraAssociationCount.push_back(0);
	}

	for (int i = 0; i < duplicateTracklets.size(); i++) {

		// Reset
		for (int j = 0; j < cameraAssociationCount.size(); j++) {
			cameraAssociationCount[j] = 0;
		}

		// Get the number of associations for each camera
		for (int j = 0; j < duplicateDetections[i].size(); j++) {
			int cameraDetectedWith = Detections[duplicateDetections[i][j]].cameraDetectedWith;
			cameraAssociationCount[cameraDetectedWith]++;
		}

		// Find the maximum number of associations of any camera
		std::vector<int>::iterator trackletsNeededIndex = std::max_element(cameraAssociationCount.begin(), cameraAssociationCount.end());
		int trackletsNeeded = cameraAssociationCount.at(std::distance(cameraAssociationCount.begin(), trackletsNeededIndex));

		// If there are more tracklets than needed
		if (duplicateTracklets[i].size() > trackletsNeeded) {

			while (duplicateTracklets[i].size() > trackletsNeeded) {

				// Find and erase the tracklet with the smallest lifetime
				int smallestLifetime = Tracklets[duplicateTracklets[i][0]].lifetime;
				int smallestLifetimeID = 0;
				for (int j = 1; j < duplicateTracklets[i].size(); j++) {
					if (Tracklets[duplicateTracklets[i][j]].lifetime < smallestLifetime) {
						smallestLifetime = Tracklets[duplicateTracklets[i][j]].lifetime;
						smallestLifetimeID = j;
					}
				}

				Tracklets[duplicateTracklets[i][smallestLifetimeID]].alive = false;
				//Tracklets.erase(Tracklets.begin() + duplicateTracklets[i][smallestLifetimeID]);
				duplicateTracklets[i].erase(duplicateTracklets[i].begin() + smallestLifetimeID);
			}
			return false;
		}
	}
	return true;
}

void Tracking::sameCameraConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	// Tracklets cannot be detected by the same camera multiple times

	// Initialise variables
	float smallestDistance = -1;
	int smallestDistanceID = -1;
	Point2f detectionPos;
	Point2f trackletPos;
	float distance;

	// Find detections with a single associated tracklet
	for (int i = 0; i < associationMatrix.rows; i++) {
		checkIfSingleAssociation(i);
	}

	std::vector<std::vector<int>> cameraAssociations;
	for (int i = 0; i < 6; i++) {
		cameraAssociations.push_back(std::vector<int>());
	}

	// Iterate through tracklets
	for (int i = 0; i < associationMatrix.cols; i++) {

		// Reset associationCount
		for (int j = 0; j < cameraAssociations.size(); j++) {
			cameraAssociations[j] = std::vector<int>();
		}

		// Increment associationCount for relevant detections
		for (int j = 0; j < associationMatrix.rows; j++) {
			if (associationMatrix.at<float>(j, i) > 0) {
				cameraAssociations[Detections[j].cameraDetectedWith].push_back(j);
			}
		}

		// Iterate through each camera
		for (int cameraID = 0; cameraID < cameraAssociations.size(); cameraID++) {

			// If this tracklet has multiple associated detections from this camera
			if (cameraAssociations[cameraID].size() > 1) {

				bool lockedDetection = false;
				bool allLocked = true;

				// Find out if there are any locked detections
				for (int j = 0; j < cameraAssociations[cameraID].size(); j++) {
					int detection = cameraAssociations[cameraID][j];
					if (associationMatrix.at<float>(detection, i) == 2) {
						lockedDetection = true;
					}
					else {
						allLocked = false;
					}
				}

				// If no locked detections or all locked detections
				if (!lockedDetection || allLocked) {

					smallestDistanceID = getClosestDetection(Detections, Tracklets[i].position, cameraAssociations[cameraID]);
					associationMatrix.at<float>(smallestDistanceID, i) = 2;

					// The closest detection is for this tracklet so remove all associations in other tracklets
					for (int j = 0; j < associationMatrix.cols; j++) {
						if (j != i) {
							if (associationMatrix.at<float>(smallestDistanceID, j) > 0) {
								associationMatrix.at<float>(smallestDistanceID, j) = 0;
							}
						}
					}

					// Remove all detections except the closest one
					for (int j = 0; j < cameraAssociations[cameraID].size(); j++) {
						int detection = cameraAssociations[cameraID][j];
						if (detection != smallestDistanceID) {
							associationMatrix.at<float>(detection, i) = 0;
							checkIfSingleAssociation(detection);
						}
					}
				}

				// If there are locked detections
				if (lockedDetection && !allLocked) {

					int detectionsRemoved = 0;
					// Remove all that aren't locked detections
					for (int j = 0; j < cameraAssociations[cameraID].size(); j++) {

						int detection = cameraAssociations[cameraID][j];
						if (associationMatrix.at<float>(detection, i) == 1) {
							associationMatrix.at<float>(detection, i) = 0;
							checkIfSingleAssociation(detection);
							detectionsRemoved++;
						}
					}

					// If still multiple remaining, remove all but closest
					if (cameraAssociations[cameraID].size() - detectionsRemoved > 1) {

						smallestDistanceID = getClosestDetection(Detections, Tracklets[i].position, cameraAssociations[cameraID]);
						associationMatrix.at<float>(smallestDistanceID, i) = 2;

						// The closest detection is for this tracklet so remove all associations in other tracklets
						for (int j = 0; j < associationMatrix.cols; j++) {
							if (j != i) {
								if (associationMatrix.at<float>(smallestDistanceID, j) > 0) {
									associationMatrix.at<float>(smallestDistanceID, j) = 0;
								}
							}
						}

						// Remove all detections except the closest one
						for (int j = 0; j < cameraAssociations[cameraID].size(); j++) {
							int detection = cameraAssociations[cameraID][j];
							if (detection != smallestDistanceID) {
								associationMatrix.at<float>(detection, i) = 0;
								checkIfSingleAssociation(detection);
							}
						}
					}
				}
			}
		}
	}
}

void Tracking::singleDetectionConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections) {

	bool multipleAssociations;
	Point2f detectionPos;
	Point2f trackletPos;
	float distance;
	float smallestDistance;
	int smallestDistanceID;

	// Check for detections with multiple associated tracklets
	// Remove all tracklets but the closest one

	// Iterate through all detections
	for (int i = 0; i < associationMatrix.rows; i++) {

		detectionPos = Detections[i].pitchPosition;
		multipleAssociations = false;
		smallestDistanceID = -1;

		// Iterate through all tracklet
		for (int j = 0; j < associationMatrix.cols; j++) {

			// If they are associated
			if (associationMatrix.at<float>(i, j) > 0) {

				// First association found
				if (smallestDistanceID == -1) {
					trackletPos = Tracklets[j].position;
					smallestDistance = sqrt(pow(detectionPos.x - trackletPos.x, 2) + pow(detectionPos.y - trackletPos.y, 2));
					smallestDistanceID = j;
				}
				// Multiple associations
				else {
					trackletPos = Tracklets[j].position;

					// Check to see if the distance is smaller
					distance = sqrt(pow(detectionPos.x - trackletPos.x, 2) + pow(detectionPos.y - trackletPos.y, 2));
					if (distance < smallestDistance) {
						smallestDistance = distance;
						smallestDistanceID = j;
					}
					multipleAssociations = true;
				}
			}
		}
		// If multiple associations found for this detection
		if (multipleAssociations) {

			// Remove all but the closest tracklet
			for (int j = 0; j < associationMatrix.cols; j++) {
				if (j != smallestDistanceID) {
					associationMatrix.at<float>(i, j) = 0;
				}
			}
		}
	}
}


void Tracking::checkIfSingleAssociation(int DetectionID) {

	int associatedTracklet = -1;
	bool detectionAssociatedOnce = true;

	// Look to see if they only have 1 association
	for (int j = 0; j < associationMatrix.cols; j++) {
		if (associationMatrix.at<float>(DetectionID, j) > 0) {
			if (associatedTracklet == -1)
				associatedTracklet = j;
			else {
				detectionAssociatedOnce = false;
				break;
			}
		}
	}

	// If it has only one association, set it as 2 to signify this
	if (detectionAssociatedOnce && associatedTracklet != -1) {
		associationMatrix.at<float>(DetectionID, associatedTracklet) = 2;
	}
}

int Tracking::getClosestDetection(const std::vector<TrackedDetection>& Detections, Point2f Point, std::vector<int>& DetectionsToCheck) {

	int smallestDistanceID = -1;
	float smallestDistance = 0;
	float distance;
	Point2f detectionPos;

	// Find the closest detection to the tracklet
	for (int i = 0; i < DetectionsToCheck.size(); i++) {

		int detection = DetectionsToCheck[i];
		detectionPos = Detections[detection].pitchPosition;

		if (smallestDistanceID == -1) {
			smallestDistance = sqrt(pow(detectionPos.x - Point.x, 2) + pow(detectionPos.y - Point.y, 2));
			smallestDistanceID = detection;
		}
		else {
			distance = sqrt(pow(detectionPos.x - Point.x, 2) + pow(detectionPos.y - Point.y, 2));
			if (distance < smallestDistance) {
				smallestDistance = distance;
				smallestDistanceID = detection;
			}
		}
	}
	return smallestDistanceID;
}


void Tracking::addColumnToMat(Mat &m) {
	
	// Create a new mat with 1 more column than m
	Mat newMat = Mat::zeros(m.rows, m.cols + 1, m.type());

	// Copy m to the new mat
	m.copyTo(newMat(Rect(Point(0, 0), m.size())));

	// Set m to the new mat
	m = newMat;
}

void Tracking::printAssociationMatrix() {

	// Printing of the association matrix for debugging purposes

	printf("   ");
	for (int i = 0; i < associationMatrix.cols; i++) {
		if (i < 10) {
			printf("%d  ", i);
		}
		else {
			printf("%d ", i);
		}
	}
	printf("\n");
	for (int i = 0; i < associationMatrix.rows; i++) {
		if (i < 10) {
			printf("%d  ", i);
		}
		else {
			printf("%d ", i);
		}
		for (int j = 0; j < associationMatrix.cols; j++) {
			printf("%d  ", (int)associationMatrix.at<float>(i, j));
		}
		printf("\n");
	}
}

bool Tracking::compareMats(const Mat& A, const Mat& B) {
	// Returns true if the mats are identical
	return (sum(A != B) == Scalar(0));
}