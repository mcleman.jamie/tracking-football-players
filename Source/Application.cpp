#include "Application.hpp"

// Application base class

Application::Application(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) {

	printf("Application initialisation\n");

	// Create window
	namedWindow("Tracking Football Players", WINDOW_FREERATIO);

	// Initialise all the cameras
	for (unsigned int i = 0; i < cameraNames.size(); i++) {
		cameras.push_back(Camera(cameraNames[i], cameraColours[i], i));
	}

	keyPressed = 0;
}


// Main application

void ApplicationMain::start() {

	printf("Main Application start\n");
	
	namedWindow("cameras", WINDOW_FREERATIO);

	// Read the aerial pitch graphic image
	pitchAerialGraphic = imread("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\pitch.jpg", IMREAD_UNCHANGED);

	// Load pitch masks for all cameras
	for (int i = 0; i < cameras.size(); i++) {
		cameras[i].loadPitchMask();
	}

	// Initialse classifier and saving
	classifier.loadHistograms();
	saver.init("detectionsData", "trackletsData");

	colourMode = 0;
	nextFrame = true;
	viewDetections = true;
	viewTracklets = true;
	currentFrame = 0;
	
	// Main loop
	while (true) {
		if (!update()) {
			break;
		}
		display();
		if (!getInput()) {
			break;
		}
	}
	
	// If a tracklet was split but both the before and after tracklets have the
	// same team classification, merge them together
	for (int i = 0; i < deadTracklets.size(); i++) {

		if (deadTracklets[i].teamSwitched) {

			int switchedToID = deadTracklets[i].switchedToID;
			bool found = false;

			// Look through dead tracklets for the tracklet switched to
			for (int j = 0; j < deadTracklets.size(); j++) {

				if (deadTracklets[j].ID == switchedToID) {
					found = true;

					// If the team classifications are the same
					if (deadTracklets[i].teamID == deadTracklets[j].teamID) {
						deadTracklets[i].mergeTracklet(deadTracklets[j]);
						i--; // Decrement i in case the merged tracklet also switched tracklets
					}
					break;
				}
			}
			// If not found, look through alive tracklets
			if (!found) {
				for (int j = 0; j < tracklets.size(); j++) {

					if (tracklets[j].ID == switchedToID) {
						found = true;

						// If the team classifications are the same
						if (deadTracklets[i].teamID == tracklets[j].teamID) {
							deadTracklets[i].mergeTracklet(tracklets[j]);
							// No need to decrement i here because an alive tracklet cannot have switched
						}
						break;
					}
				}
			}
		}
	}

	// Save the tracklet data to a file
	saver.saveTracklets(tracklets, deadTracklets);
}

bool ApplicationMain::update() {

	if (nextFrame) {
		
		currentFrame++;
		printf("Current frame: %d\n", currentFrame);

		detector.resetDetections();

		// Iterate through each camera
		for (unsigned int i = 0; i < cameras.size(); i++) {

			// Load the next frame
			if (!cameras[i].nextFrame()) {
				printf("ERROR: reading camera %s at frame %d\n", cameras[i].name.c_str(), i);
				return false;
			}

			// Remove the background
			cameras[i].removeBackground();

			// Detect players from extracted foreground
			detector.getNewDetections(cameras[i].foreground, cameras[i].id);

		}

		// Update the tracked detections and validate the tracked detections so that they are within the frame
		detector.update(trackedDetections);
		detector.validateTrackedDetections(trackedDetections, cameras[0].currentFrame.cols, cameras[0].currentFrame.rows);

		// Iterate through all the tracked detections
		for (int i = 0; i < trackedDetections.size(); i++) {

			// Get teamID if not occluded
			if (!trackedDetections[i].occluded) {
				Mat playerMat = cameras[trackedDetections[i].cameraDetectedWith].currentFrame(trackedDetections[i].boundingBox);
				trackedDetections[i].teamID = classifier.classify(playerMat, trackedDetections[i].teamPercentages);
			}

			// Get pitch positions
			trackedDetections[i].pitchPosition = cameras[trackedDetections[i].cameraDetectedWith].convertImageCoordToPitchCoord(trackedDetections[i].boundingBox);
		}

		// Update tracking
		playerTracking.update(currentFrame, tracklets, trackedDetections);

		// Switch to new tracklets and remove dead tracklets
		for (int i = 0; i < tracklets.size(); i++) {

			// If the team has switched
			if (tracklets[i].teamSwitched) {

				// Create a new tracklet for 
				TrackedPlayer newTracklet;
				tracklets[i].switchToNewTracklet(newTracklet, currentFrame);

				// Remove the dead tracklet
				deadTracklets.push_back(tracklets[i]);
				tracklets.erase(tracklets.begin() + i);
				i--;

				// Add the new tracklet
				tracklets.push_back(newTracklet);
			}

			// If the tracklet is dead 
			else if (!tracklets[i].alive) {

				// If it's lifetime is greater than 25 (1 second)
				if (tracklets[i].lifetime > 25) {

					// Add to dead tracklets vector to save later
					deadTracklets.push_back(tracklets[i]);
				}
				// Remove from alive tracklets vector
				tracklets.erase(tracklets.begin() + i);
				i--;
			}
		}

		// Save the detections in this frame
		saver.saveDetections(currentFrame, trackedDetections);
	}
	return true;
}

void ApplicationMain::display() {
	
	// Copy empty aerial view to pitchWithPlayers mat
	pitchAerialGraphic.copyTo(pitchWithPlayers);
	Scalar colour = Scalar(0, 0, 0);
	Point2f pitchCoords;

	// Display the detections of each camera
	if (viewDetections) {
		for (int i = 0; i < trackedDetections.size(); i++) {

			// Display the detections as the colour of the classification
			if (colourMode == 1) {
				if (trackedDetections[i].teamID == 0)
					colour = Scalar(255.0, 255.0, 255.0); // White
				else if (trackedDetections[i].teamID == 1)
					colour = Scalar(255.0, 0, 0); // Blue
				else if (trackedDetections[i].teamID == 2)
					colour = Scalar(0, 0, 255.0); // Red
				else if (trackedDetections[i].teamID == 3)
					colour = Scalar(0, 0, 0); // Black
				else if (trackedDetections[i].teamID == 4)
					colour = Scalar(0, 255.0, 255.0); // Yellow
			}
			// Display the detections as the colour of the camera detected with
			else {
				colour = cameras[trackedDetections[i].cameraDetectedWith].colour;
			}

			// Get pitch coords and scale to the size of the image
			pitchCoords = trackedDetections[i].pitchPosition;
			pitchCoords.x = pitchCoords.x * 10.0 + 20.0;
			pitchCoords.y = pitchCoords.y * 10.0 + 20.0;

			// Display as filled in circle
			circle(pitchWithPlayers, pitchCoords, 10, colour, -1);
		}
	}
	
	// Display the tracklets
	if (viewTracklets) {

		for (int i = 0; i < tracklets.size(); i++) {

			// Find the colour of the tracklet classification
			if (tracklets[i].teamID == 0)
				colour = Scalar(255.0, 255.0, 255.0); // White
			else if (tracklets[i].teamID == 1)
				colour = Scalar(255.0, 0, 0); // Blue
			else if (tracklets[i].teamID == 2)
				colour = Scalar(0, 0, 255.0); // Red
			else if (tracklets[i].teamID == 3)
				colour = Scalar(0, 0, 0); // Black
			else if (tracklets[i].teamID == 4)
				colour = Scalar(0, 255.0, 255.0); // Yellow

			// Get pitch coords and scale to the size of the image
			pitchCoords = tracklets[i].position;
			pitchCoords.x = pitchCoords.x * 10.0 + 20.0;
			pitchCoords.y = pitchCoords.y * 10.0 + 20.0;

			// Display as a hollow circle
			circle(pitchWithPlayers, pitchCoords, 10, colour, 2);
		}
	}

	// Show 
	imshow("Tracking Football Players", pitchWithPlayers);

	// Display current frame of all cameras with detections in a single window
	Mat displayImage = Mat::zeros(cameras[0].currentFrame.rows * 2, cameras[0].currentFrame.cols * 3, CV_8UC3);
	Mat currentFrame;
	int x, y;
	int width = displayImage.cols / 3;
	int height = displayImage.rows / 2;
	Rect ROI;

	// Camera order. Top left, bottom left, top middle, bottom middle, top right, bottom right
	std::vector<int> cameraOrder;
	cameraOrder.push_back(5);
	cameraOrder.push_back(4);
	cameraOrder.push_back(3);
	cameraOrder.push_back(2);
	cameraOrder.push_back(1);
	cameraOrder.push_back(0);

	int cameraNum = 0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {

			// Get the frame of the current camera
			currentFrame = cameras[cameraOrder[cameraNum]].currentFrame;

			// Iterate through detections to draw on the current camera
			for (int d = 0; d < trackedDetections.size(); d++) {
				if (trackedDetections[d].cameraDetectedWith == cameraOrder[cameraNum]) {
					if (trackedDetections[d].teamID == 0)
						colour = Scalar(255.0, 255.0, 255.0);
					else if (trackedDetections[d].teamID == 1)
						colour = Scalar(255.0, 0, 0);
					else if (trackedDetections[d].teamID == 2)
						colour = Scalar(0, 0, 255.0);
					else if (trackedDetections[d].teamID == 3)
						colour = Scalar(0, 0, 0);
					else if (trackedDetections[d].teamID == 4)
						colour = Scalar(0, 255.0, 255.0);
					rectangle(currentFrame, trackedDetections[d].boundingBox, colour, 3);
				}
			}

			// Calculate the current cameras x and y position in the display image
			x = i * width;
			y = j * height;
			
			// Draw a rectangle around the current camera frame of the colour of the camera
			rectangle(currentFrame, Rect(0, 0, cameras[cameraOrder[cameraNum]].currentFrame.cols, cameras[cameraOrder[cameraNum]].currentFrame.rows), cameras[cameraOrder[cameraNum]].colour, 10);

			// Copy the current frame camera to the displayImage
			ROI = Rect(x, y, cameras[cameraOrder[cameraNum]].currentFrame.cols, cameras[cameraOrder[cameraNum]].currentFrame.rows);
			currentFrame.copyTo(displayImage(ROI));
			cameraNum++; // Go to next camera
		}
	}
	// Show
	imshow("cameras", displayImage);
}

bool ApplicationMain::getInput() {

	// Wait for a ms to get input
	keyPressed = waitKey(1);

	// If ESCAPE is pressed, exit the program
	if (keyPressed == 27) {
		printf("ESCAPE pressed\n");
		return false;
	}
	// If the spacebar is pressed, go to next frame
	if (keyPressed == 32) {
		nextFrame = true;
	}
	// If A is pressed, change colour mode
	else if (keyPressed == 97) {
		if (colourMode == 0)
			colourMode = 1;
		else
			colourMode = 0;
		nextFrame = false;
	}
	// If S is pressed, toggle viewDetections mode
	else if (keyPressed == 115) {
		viewDetections = !viewDetections;
		nextFrame = false;
	}
	// If D is pressed, toggle viewTracklets mode
	else if (keyPressed == 100) {
		viewTracklets = !viewTracklets;
		nextFrame = false;
	}
	// If G is pressed, save the current frame
	else if (keyPressed == 103) {
		saveFrame();
		nextFrame = false;
	}
	return true;
}

void ApplicationMain::saveFrame() {
	// Save the pitch with players mat to a jpg file
	imwrite("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\mainApplicationSS.jpg", pitchWithPlayers);
}


// View detections application

void ApplicationViewDetections::start() {

	// Initialise variables
	currentCamera = 1;
	nextFrame = true;
	currentFrame = 0;

	// Load pitch masks
	for (int i = 0; i < cameras.size(); i++) {
		cameras[i].loadPitchMask();
	}

	// Initialise classifier
	classifier.loadHistograms();

	// Main loop
	while (true) {
		if (nextFrame) {

			currentFrame++;
			printf("Frame number: %d\n", currentFrame);

			detector.resetDetections();
			// Iterate through each camera
			for (unsigned int i = 0; i < cameras.size(); i++) {

				// Load the next frame
				if (!cameras[i].nextFrame()) {
					printf("ERROR: reading camera %s at frame %d\n", cameras[i].name.c_str(), i);
					return;
				}

				// Remove the background
				cameras[i].removeBackground();

				// Detect players
				detector.getNewDetections(cameras[i].foreground, cameras[i].id);
			}

			// Update the tracked detections and validate the tracked detections so that they are within the frame
			detector.update(trackedDetections);
			detector.validateTrackedDetections(trackedDetections, cameras[0].currentFrame.cols, cameras[0].currentFrame.rows);

			for (int i = 0; i < trackedDetections.size(); i++) {
				
				// Classify detections
				Mat playerMat = cameras[trackedDetections[i].cameraDetectedWith].currentFrame(trackedDetections[i].boundingBox);
				trackedDetections[i].teamID = classifier.classify(playerMat, trackedDetections[i].teamPercentages);
			}

			nextFrame = false;
		}

		display(); // Display

		// Wait until a key is pressed
		keyPressed = waitKey(0);

		// If ESCAPE is pressed, exit the program
		if (keyPressed == 27) {
			printf("ESCAPE pressed\n");
			return;
		}
		// If a number between 1 and 6 is pressed, set camera
		else if (keyPressed >= 49 && keyPressed <= 54) {
			currentCamera = keyPressed - 48;
		}
		// If the spacebar is pressed, go to next frame
		else if (keyPressed == 32) {
			nextFrame = true;
		}
		// If S is pressed, save frame
		else if (keyPressed == 115) {
			saveFrame();
		}
	}
}

void ApplicationViewDetections::display() {

	// Get current frame
	frame = cameras[currentCamera - 1].currentFrame;
	Scalar colour = Scalar(0, 0, 0);

	// Draw boxes of all the non-tracked detections for this frame in black
	for (int i = 0; i < detector.detections.size(); i++) {
		if (detector.detections[i].cameraDetectedWith == currentCamera - 1) {
			rectangle(frame, detector.detections[i].boundingBox, colour, 2);
		}
	}

	// Draw boxes of all the tracked detections
	for (int i = 0; i < trackedDetections.size(); i++) {

		if (trackedDetections[i].cameraDetectedWith == currentCamera - 1) {

			// Get the colour of the team this detection is classified as
			if (trackedDetections[i].teamID == 0)
				colour = Scalar(255.0, 255.0, 255.0);
			else if (trackedDetections[i].teamID == 1) {
				colour = Scalar(255.0, 0, 0);
			}
			else if (trackedDetections[i].teamID == 2) {
				colour = Scalar(0, 0, 255.0);
			}
			rectangle(frame, trackedDetections[i].boundingBox, colour, 2);
		}
	}
	// Show
	imshow("Tracking Football Players", frame);
}

void ApplicationViewDetections::saveFrame() {
	// Save the current frame to a jpg file
	imwrite("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\viewDetectionsSS.jpg", frame);
}


// View background subtraction application

void ApplicationViewBackgroundSubtraction::start() {

	// Initialise variables
	currentCamera = 1;
	nextFrame = true;
	isSubtraction = false;

	// Load pitch masks
	for (int i = 0; i < cameras.size(); i++) {
		cameras[i].loadPitchMask();
	}

	while (true) {

		if (nextFrame) {

			// Iterate through each camera
			for (unsigned int i = 0; i < cameras.size(); i++) {

				// Get the next frame
				if (!cameras[i].nextFrame()) {
					printf("ERROR: reading camera %s at frame %d\n", cameras[i].name.c_str(), i);
					return;
				}

				// Remove the background
				cameras[i].removeBackground();
			}
			nextFrame = false;
		}

		display(); // Display

		// Wait until a key is pressed
		keyPressed = waitKey(0);

		// If ESCAPE is pressed, exit the program
		if (keyPressed == 27) {
			printf("ESCAPE pressed\n");
			return;
		}
		// If a number between 1 and 6 is pressed, change camera
		else if (keyPressed >= 49 && keyPressed <= 54) {
			currentCamera = keyPressed - 48;
		}
		// If the spacebar is pressed, go to next frame
		else if (keyPressed == 32) {
			nextFrame = true;
		}
		// If q is pressed, toggle between showing frame or foreground
		else if (keyPressed == 113) {
			isSubtraction = !isSubtraction;
		}
		// If S is pressed, save frame
		else if (keyPressed == 115) {
			saveFrame();
		}
	}
}

void ApplicationViewBackgroundSubtraction::display() {

	// If we want to show the foreground
	if (isSubtraction) {
		frame = cameras[currentCamera - 1].foreground;
	}

	// If we want to show the current frame
	else {
		frame = cameras[currentCamera - 1].currentFrame;
	}

	imshow("Tracking Football Players", frame);
}

void ApplicationViewBackgroundSubtraction::saveFrame() {
	// Save the current frame to a jpg file
	imwrite("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\viewForegroundSS.jpg", frame);
}


// Create pitch masks application

void ApplicationCreatePitchMask::start() {

	Point pitchCoord;
	Mat pitchMask;

	// Iterate through each camera
	for (unsigned int i = 0; i < cameras.size(); i++) {

		printf("Creating pitch mask for camera: %s\n", cameras[i].name.c_str());

		// Get the next frame and subtract background
		if (!cameras[i].nextFrame()) {
			printf("ERROR: reading camera %s at frame %d\n", cameras[i].name.c_str(), i);
			return;
		}

		// Create an initial pitch mask full of zeros
		pitchMask = Mat::zeros(cameras[i].currentFrame.rows, cameras[i].currentFrame.cols, CV_8UC1);

		uchar* p;

		// Iterate through each row of the mask
		for (int y = 0; y < pitchMask.rows; y++) {

			p = pitchMask.ptr<uchar>(y); // Store row as a pointer

			// Iterate through each column of the mask
			for (int x = 0; x < pitchMask.cols; x++) {

				// Get pitch coord for this coord of the image
				pitchCoord = cameras[i].convertImageCoordToPitchCoord(Point(x, y));

				// If the pitch coord is in the actual pitch region
				if (pitchCoord.x >= 0 && pitchCoord.x <= 115) {
					if (pitchCoord.y >= 0 && pitchCoord.y <= 74) {
						p[x] = 255; // Set it to white
					}
				}
			}
		}

		// Save this image as a pitch mask
		imwrite("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\pitchMask" + cameras[i].name + ".jpg", pitchMask);
		cameras[i].loadPitchMask(); // Load it in
	}

	printf("All pitch masks created\n");

	int keyPressed = 0;
	currentCamera = 1;

	while (true) {

		// Display
		display();

		// Wait until a key is pressed
		keyPressed = waitKey(0);

		// If ESCAPE is pressed, exit the program
		if (keyPressed == 27) {
			printf("ESCAPE pressed\n");
			return;
		}
		// If a number between 1 and 6 is pressed, change camera
		else if (keyPressed >= 49 && keyPressed <= 54) {
			currentCamera = keyPressed - 48;
		}
	}
}

void ApplicationCreatePitchMask::display() {

	// Display the pitch mask for the current camera
	imshow("Tracking Football Players", cameras[currentCamera - 1].pitchMask);
}


// Create histograms application

void ApplicationCreateHistograms::start() {

	printf("Creating Histograms application started\n");

	// Initialise variables
	bool nextFrame = true;
	int currentPlayer = 0;

	// Load pitch masks
	for (int i = 0; i < cameras.size(); i++) {
		cameras[i].loadPitchMask();
	}

	// Set the team names of each class
	teamNames.push_back("white");
	teamNames.push_back("blue");
	teamNames.push_back("officials");

	// Create 3 empty histograms for each class, one for each BGR value
	for (int i = 0; i < teamNames.size(); i++) {
		std::vector<Mat> averageBgrMats;
		for (int j = 0; j < 3; j++) {
			averageBgrMats.push_back(Mat::zeros(256, 1, CV_32FC1));
		}
		histograms.push_back(averageBgrMats);
	}

	// Main loop
	while (true) {
		if (nextFrame) {

			detector.resetDetections();

			// Iterate through each camera
			for (unsigned int i = 0; i < cameras.size(); i++) {

				// Get the next frame
				if (!cameras[i].nextFrame()) {
					printf("ERROR: reading camera %s at frame %d\n", cameras[i].name.c_str(), i);
					return;
				}
				// Remove background
				cameras[i].removeBackground();

				// Detect players
				detector.getNewDetections(cameras[i].foreground, cameras[i].id);
			}

			// Update the tracked detections and validate the tracked detections so that they are within the frame
			detector.update(trackedDetections);
			detector.validateTrackedDetections(trackedDetections, cameras[0].currentFrame.cols, cameras[0].currentFrame.rows);

			nextFrame = false;
		}

		// Get the mat for the current player
		player = cameras[trackedDetections[currentPlayer].cameraDetectedWith].currentFrame(trackedDetections[currentPlayer].boundingBox);

		display(); // Display

		// Wait until a key is pressed
		keyPressed = waitKey(0);

		// If ESCAPE is pressed, exit the program
		if (keyPressed == 27) {
			printf("ESCAPE pressed\n");
			return;
		}
		// If the spacebar is pressed, ignore this detection
		else if (keyPressed == 32) {
			if (currentPlayer + 1 > trackedDetections.size() - 1) {
				currentPlayer = 0;
				nextFrame = true;
			}
			else {
				currentPlayer++;
			}
		}
		// If A is pressed, train player as class 0 (white team)
		else if (keyPressed == 97) {
			trainPlayer(0);
			if (currentPlayer + 1 > trackedDetections.size() - 1) {
				currentPlayer = 0;
				nextFrame = true;
			}
			else {
				currentPlayer++;
			}
		}
		// If B is pressed, train player as class 1 (blue team)
		else if (keyPressed == 98) {
			trainPlayer(1);
			if (currentPlayer + 1 > trackedDetections.size() - 1) {
				currentPlayer = 0;
				nextFrame = true;
			}
			else {
				currentPlayer++;
			}
		}
		// If C is pressed, train player as class 2 (officials)
		else if (keyPressed == 99) {
			trainPlayer(2);
			if (currentPlayer + 1 > trackedDetections.size() - 1) {
				currentPlayer = 0;
				nextFrame = true;
			}
			else {
				currentPlayer++;
			}
		}
		// If D is pressed, store the histograms
		else if (keyPressed == 100) {
			storeHistograms();
			if (currentPlayer + 1 > trackedDetections.size() - 1) {
				currentPlayer = 0;
				nextFrame = true;
			}
			else {
				currentPlayer++;
			}
		}
	}
}

void ApplicationCreateHistograms::display() {
	// Display the player so the user can decide on the classification
	imshow("Tracking Football Players", player);
}

void ApplicationCreateHistograms::trainPlayer(int teamID) {

	// Assert that the teamID is valid
	if (teamID > histograms.size() - 1) {
		return;
	}

	// Split player mat into mats for b, g, r values and convert to 32F
	std::vector<Mat> bgrPlayers;
	split(player, bgrPlayers);

	bgrPlayers[0].convertTo(bgrPlayers[0], CV_32F);
	bgrPlayers[1].convertTo(bgrPlayers[1], CV_32F);
	bgrPlayers[2].convertTo(bgrPlayers[2], CV_32F);

	// Create a 2d Guassian kernel for weighting the centre pixels more than outside pixels
	Mat xKernel = getGaussianKernel(player.cols, player.cols/ 10, CV_32F);
	Mat yKernel = getGaussianKernel(player.rows, player.rows / 10, CV_32F);
	Mat kernel = yKernel * xKernel.t();

	// Calculate the histograms
	for (int i = 0; i < bgrPlayers.size(); i++) {
		for (int y = 0; y < bgrPlayers[i].rows; y++) {
			for (int x = 0; x < bgrPlayers[i].cols; x++) {
				histograms[teamID][i].at<float>((int)bgrPlayers[i].at<float>(y, x)) += kernel.at<float>(y, x);
			}
		}
	}
}

void ApplicationCreateHistograms::storeHistograms() {

	// Open the histograms data text file
	std::ofstream dataFile;
	dataFile.open("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\Histograms.txt");

	// For each histogram
	for (int i = 0; i < histograms.size(); i++) {

		// Write the name of the class
		dataFile << teamNames[i].c_str() << "\n";

		// For each bin, write the value for each b, g, r value
		for (int j = 0; j < 256; j++) {
			dataFile << histograms[i][0].at<float>(j) << "\n";
			dataFile << histograms[i][1].at<float>(j) << "\n";
			dataFile << histograms[i][2].at<float>(j) << "\n";
		}
	}

}


// Creating background application

void ApplicationCreateBackgrounds::start() {

	// Initialise variables
	int totalFrames;
	int currentFrame = 0;
	int frameIncrement = 0;
	int numberOfFramesToAverage = 10;

	// Iterate through all the cameras
	for (int i = 0; i < cameras.size(); i++) {

		printf("camera: %d\n", i);

		frames.clear(); // Clear the frames vector

		// Calculate the interval of the frames to increment
		totalFrames = cameras[i].video.get(CAP_PROP_FRAME_COUNT);
		frameIncrement = totalFrames / numberOfFramesToAverage;

		// Load the first frame
		cameras[i].nextFrame();

		// Store frames, up to the total number we want
		for (int j = 0; j < numberOfFramesToAverage; j++) {

			if (currentFrame < totalFrames) {

				// Calculate the current frame we want
				currentFrame = j * frameIncrement;

				// Load this frame
				cameras[i].readFrame(currentFrame);

				// Store this in the back of the frames vector
				frames.push_back(Mat());
				cameras[i].currentFrame.copyTo(frames.back());
			}
		}

		// Calculate the median for all the frames of this camera
		getMedian();
		
		// Store the background to a file
		imwrite("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\bg" + cameras[i].name + ".jpg", median);
	}
}

void ApplicationCreateBackgrounds::display() {
	return;
}

void ApplicationCreateBackgrounds::getMedian() {

	// Create empty mat to store the median
	median = Mat::zeros(frames[0].rows, frames[0].cols, CV_8UC3);

	std::vector<int> bValues;
	std::vector<int> gValues;
	std::vector<int> rValues;
	Vec3b temp;

	// Iterate through all the pixels in the frame
	for (int x = 0; x < frames[0].cols; x++) {
		for (int y = 0; y < frames[0].rows; y++) {

			bValues.clear();
			gValues.clear();
			rValues.clear();

			// For each frame, store the blue, green and red values into vectors
			for (int i = 0; i < frames.size(); i++) {
				temp = frames[i].at<Vec3b>(y, x);
				bValues.push_back(temp[0]);
				gValues.push_back(temp[1]);
				rValues.push_back(temp[2]);
			}

			// Calculate the median of the blue, green and red vectors
			std::nth_element(bValues.begin(), bValues.begin() + bValues.size() / 2, bValues.end());
			std::nth_element(gValues.begin(), gValues.begin() + gValues.size() / 2, gValues.end());
			std::nth_element(rValues.begin(), rValues.begin() + rValues.size() / 2, rValues.end());
			temp[0] = bValues[bValues.size() / 2];
			temp[1] = gValues[gValues.size() / 2];
			temp[2] = rValues[rValues.size() / 2];

			// Save these values to the median mat
			median.at<Vec3b>(y, x) = temp;
		}
	}
}


// Displaying the saved data application

void ApplicationDisplaySavedData::start() {

	printf("Enter 1 to view save\n");
	printf("Enter 2 to output save as video\n");

	// Get input from user to view save or output as video
	char input;
	while (true) {
		std::cin >> input;
		if (input == 49) { // 1
			saveVideo = false;
			break;
		}
		else if (input == 50) { // 2
			saveVideo = true;
			break;
		}
	}

	// Create file names
	detectionsFilename = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\detectionsData.txt";
	trackletsFilename = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\trackletsData.txt";

	// Load detections file 
	detectionsFile = std::fstream(detectionsFilename);
	if (!detectionsFile.is_open()) {
		printf("Couldn't open detections file\n");
		return;
	}
	// Load player tracklets
	if (!loadPlayerTracklets()) {
		printf("Couldn't open tracklets file\n");
		return;
	}

	// Load aerial pitch graphic
	pitchAerialGraphic = imread("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\pitch.jpg", IMREAD_UNCHANGED);	
	
	int numTracklets = 0;
	paused = false;
	int fps = 25;
	float timePerFrame = 1000 / fps;
	currentCamera = 0;

	int64 startTicks = getTickCount();
	int64 endTicks = 0;
	int64 timeToWait = 0;

	// If we're saving the video
	if (saveVideo) {

		// Load first frame of all cameras
		for (int i = 0; i < cameras.size(); i++) {
			cameras[i].nextFrame();
		}
		
		// Create VideoWriter objects for aerial view
		std::string videoName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\output.avi";
		aerialVideo = VideoWriter(videoName, VideoWriter::fourcc('M', 'P', 'E', 'G'), 25, Size(pitchAerialGraphic.cols, pitchAerialGraphic.rows), true);

		// Create VideoWriter objects for each camera
		for (int i = 0; i < cameras.size(); i++) {
			videoName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\";
			videoName += cameras[i].name + "detections.avi";
			detectionVideos.push_back(VideoWriter(videoName, VideoWriter::fourcc('M', 'P', 'E', 'G'), 25, Size(cameras[i].currentFrame.cols, cameras[i].currentFrame.rows), true));
		}
	}
	else {
		namedWindow("cameras", WINDOW_FREERATIO);
	}



	bool cameraReadSuccessful = true;

	while (true) {

		printf("Current frame: %d\n", currentFrame);

		// If we are saving the video
		if (saveVideo) {

			// Load detections for the next frame
			if (!loadDetections(-1))
				break;

			// Load frames of all the cameras
			for (int i = 0; i < cameras.size(); i++) {
				if (!cameras[i].readFrame(currentFrame)) {
					printf("Error reading frame\n");
					cameraReadSuccessful = false;
				}
			}

			// If camera reading was unsuccesful, break the loop
			if (!cameraReadSuccessful)
				break;

			// Display (save)
			display();
			keyPressed = waitKey(1);
		}

		// If we are displaying the save
		else {

			// If not paused
			if (!paused) {

				startTicks = getTickCount();

				// Load detections for the next frame
				if (!loadDetections(-1))
					break;

				// Display
				display();

				// Calculate time needed to wait so that it plays at 25 fps
				endTicks = getTickCount();
				timeToWait = timePerFrame - ((endTicks - startTicks) / getTickFrequency());

				// If less than 0, wait minimum time possible
				if (timeToWait < 0) {
					timeToWait = 1;
				}
				// Wait for timeToWait ms
				keyPressed = waitKey(timeToWait);
			}
			// If paused
			else {

				// Load detections for the current frame
				loadDetections(currentFrame);

				// Load current camera for the current frame
				cameras[currentCamera].readFrame(currentFrame);

				// Display
				display();

				// Wait until user input
				keyPressed = waitKey(0);
			}
		}


		// If ESCAPE is pressed, exit the program
		if (keyPressed == 27) {
			printf("ESCAPE pressed\n");
			break;
		}
		// If space is pressed, pause it
		if (keyPressed == 32) {
			paused = !paused;
		}

		// Moving frame by frame, can only do if paused
		if (paused) {
			// If A is pressed, go backwards
			if (keyPressed == 97) {
				currentFrame--;
			}
			// If S is pressed, go forwards
			if (keyPressed == 115) {
				currentFrame++;
			}
		}

		// If a number between 1 and 6 is pressed
		if (keyPressed >= 49 && keyPressed <= 54) {
			currentCamera = keyPressed - 49;
		}
	}

	// Close detections file
	detectionsFile.close();

	// If we are saving, release the VideoWriter objects
	if (saveVideo) {
		aerialVideo.release();
		for (int i = 0; i < detectionVideos.size(); i++) {
			detectionVideos[i].release();
		}
	}
}

void ApplicationDisplaySavedData::display() {

	Mat pitchWithPlayers;
	pitchAerialGraphic.copyTo(pitchWithPlayers);
	Scalar colour = Scalar(0, 0, 255.0); // Red
	Point2f pitchCoords;
	Point2f previousPoint;
	Point2f currentPoint;
	int numLines = 0;

	// Aerial view graphic
	for (int i = 0; i < playerTracklets.size(); i++) {

		// Check that the current tracklet is tracked at the current frame
		if (currentFrame >= playerTracklets[i].firstFrameDetected) {
			if (currentFrame < playerTracklets[i].firstFrameDetected + playerTracklets[i].lifetime) {

				if (playerTracklets[i].teamID == 0)
					colour = Scalar(255.0, 255.0, 255.0);
				else if (playerTracklets[i].teamID == 1)
					colour = Scalar(255.0, 0, 0);
				else if (playerTracklets[i].teamID == 2)
					colour = Scalar(0, 0, 255.0);
				else if (playerTracklets[i].teamID == 3)
					colour = Scalar(0, 0, 0);
				else if (playerTracklets[i].teamID == 4)
					colour = Scalar(0, 255.0, 255.0);

				pitchCoords = playerTracklets[i].positionHistory[currentFrame - playerTracklets[i].firstFrameDetected];
				pitchCoords.x = pitchCoords.x * 10.0 + 20.0;
				pitchCoords.y = pitchCoords.y * 10.0 + 20.0;
				circle(pitchWithPlayers, pitchCoords, 10, colour, 2);

				// Draw a 25 frame long trail of all the tracklets
				numLines = 0;
				// Start from most recent and go backwards
				for (int j = currentFrame - playerTracklets[i].firstFrameDetected - 1; j >= 1; j--) {

					if (numLines >= 25) {
						break;
					}

					// Get point at index j
					previousPoint = playerTracklets[i].positionHistory[j] * 10.0;
					previousPoint.x += 20.0;
					previousPoint.y += 20.0;

					// Get point at index j - 1
					currentPoint = playerTracklets[i].positionHistory[j - 1] * 10.0;
					currentPoint.x += 20.0;
					currentPoint.y += 20.0;

					line(pitchWithPlayers, previousPoint, currentPoint, Scalar(0, 0, 0), 2);
					numLines++;
				}
			}
		}
	}

	// Display detections if we are paused for the current camera
	if (paused && !saveVideo) {

		Mat frame = cameras[currentCamera].currentFrame;
		colour = Scalar(0, 0, 0);

		for (int i = 0; i < trackedDetections.size(); i++) {
			if (trackedDetections[i].cameraDetectedWith == currentCamera) {
				if (trackedDetections[i].teamID == 0)
					colour = Scalar(255.0, 255.0, 255.0);
				else if (trackedDetections[i].teamID == 1) {
					colour = Scalar(255.0, 0, 0);
				}
				else if (trackedDetections[i].teamID == 2) {
					colour = Scalar(0, 0, 255.0);
				}
				rectangle(frame, trackedDetections[i].boundingBox, colour, 2);
			}
		}
		imshow("cameras", frame);
	}

	// Get detections for every camera and write to the video
	if (saveVideo) {

		Mat frame;
		for (int i = 0; i < cameras.size(); i++) {

			cameras[i].currentFrame.copyTo(frame);
			colour = Scalar(0, 0, 0);

			for (int j = 0; j < trackedDetections.size(); j++) {
				if (trackedDetections[j].cameraDetectedWith == i) {
					if (trackedDetections[j].teamID == 0)
						colour = Scalar(255.0, 255.0, 255.0);
					else if (trackedDetections[j].teamID == 1) {
						colour = Scalar(255.0, 0, 0);
					}
					else if (trackedDetections[j].teamID == 2) {
						colour = Scalar(0, 0, 255.0);
					}
					rectangle(frame, trackedDetections[j].boundingBox, colour, 2);
				}
			}

			detectionVideos[i].write(frame);
		}

		aerialVideo.write(pitchWithPlayers);
	}
	else {
		imshow("Tracking Football Players", pitchWithPlayers);
	}

}

bool ApplicationDisplaySavedData::loadPlayerTracklets() {

	// Open tracklets file
	std::fstream trackletsFile = std::fstream(trackletsFilename);
	if (!trackletsFile.is_open()) {
		return false;
	}

	int startFrame;
	int lifetime;
	int teamID;
	Point2f position;
	SavedPlayer player;

	// Loop until we reach end of file
	while (true) {

		// Get first entry of line which is the start frame
		trackletsFile >> startFrame;

		// If it's the end of file, break from the loop
		if (trackletsFile.eof()) {
			break;
		}
		
		// Read lifetime and teamID
		trackletsFile >> lifetime;
		trackletsFile >> teamID;

		// Create SavedPlayer object and initialise
		player = SavedPlayer();
		player.firstFrameDetected = startFrame;
		player.lifetime = lifetime;
		player.teamID = teamID;

		// Get all the positions and add to the position history vector
		for (int i = 0; i < lifetime; i++) {
			trackletsFile >> position.x;
			trackletsFile >> position.y;
			player.positionHistory.push_back(position);
		}

		// Add the player to the player tracklets vector
		playerTracklets.push_back(player);
	}

	// Close file
	trackletsFile.close();
	return true;
}

bool ApplicationDisplaySavedData::loadDetections(int Frame) {

	// If -1 is passed, get the next frame
	if (Frame == -1) {
		Frame = currentFrame + 1;
	}

	int numDetections;
	Rect boundingBox;
	int cameraDetectedWith;
	int detectionTeamID;

	// Get the current frame of the current line
	detectionsFile >> currentFrame;

	// If we haven't reached the end
	if (!detectionsFile.eof()) {

		// If the currentFrame is ahead of the desired frame
		if (currentFrame > Frame) {

			// Go back to the beginning and loop to the desired frame
			detectionsFile.seekg(std::ios::beg);
			for (int i = 1; i < Frame; i++) {
				detectionsFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
			detectionsFile >> currentFrame;
		}

		// If the current frame is ahead of the desired frame
		else if (currentFrame < Frame) {

			// Loop to the desired frame
			for (int i = 1; i < Frame - currentFrame; i++) {
				detectionsFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
			detectionsFile >> currentFrame;
		}

		// If we are on the correct frame
		if (currentFrame == Frame) {

			// Clear the trackedDetections vector
			trackedDetections.clear();
			detectionsFile >> numDetections;

			// Iterate through the number of detections for this frame
			for (int i = 0; i < numDetections; i++) {

				detectionsFile >> cameraDetectedWith;
				detectionsFile >> boundingBox.x;
				detectionsFile >> boundingBox.y;
				detectionsFile >> boundingBox.width;
				detectionsFile >> boundingBox.height;
				detectionsFile >> detectionTeamID;

				// Create a new tracked detection object and add it to the vector
				trackedDetections.push_back(TrackedDetection(boundingBox, cameraDetectedWith));
				trackedDetections.back().teamID = detectionTeamID;
			}
		}
	}
	else {
		return false;
	}
	return true;
}
