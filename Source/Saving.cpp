#include "Saving.hpp"

Saving::Saving() {
	// Initialise file names with default values
	detectionsFileName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\detectionsData.txt";
	trackletsFileName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\trackletsData.txt";
}

void Saving::init(std::string DetectionsFileName, std::string TrackletsFileName) {

	// Set file names
	detectionsFileName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\" + DetectionsFileName + ".txt";
	trackletsFileName = "C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Output\\" + TrackletsFileName + ".txt";

	// Open detections file and write an empty string to clear it
	std::ofstream dataFile;
	dataFile.open(detectionsFileName);
	dataFile << "";
	dataFile.close();

	// Open tracklets file and write an empty string to clear it
	dataFile.open(trackletsFileName);
	dataFile << "";
	dataFile.close();
}

void Saving::saveDetections(int FrameNumber, const std::vector<TrackedDetection> Detections) {

	// Open detections file
	std::ofstream dataFile;
	dataFile.open(detectionsFileName, std::ios_base::app);

	// Write frame number and number of detections
	dataFile << FrameNumber << " ";
	dataFile << Detections.size() << " ";

	// For every detection, write:
	// cameraDetectedWith x y width height teamID
	for (int i = 0; i < Detections.size(); i++) {
		dataFile << Detections[i].cameraDetectedWith << " ";
		dataFile << Detections[i].boundingBox.x << " ";
		dataFile << Detections[i].boundingBox.y << " ";
		dataFile << Detections[i].boundingBox.width << " ";
		dataFile << Detections[i].boundingBox.height << " ";
		dataFile << Detections[i].teamID << " ";
	}
	// Write a newline character and close file
	dataFile << "\n";
	dataFile.close();
}

void Saving::saveTracklets(const std::vector<TrackedPlayer> aliveTracklets, const std::vector<TrackedPlayer> deadTracklets) {

	// Open tracklets file
	std::ofstream dataFile;
	dataFile.open(trackletsFileName, std::ios_base::app);

	// For every tracklet, write:
	// firstFrameDetected lifetime teamID {pos[0].x pos[0].y pos[1].x pos[1].y ...} \n

	// Alive tracklets
	for (int i = 0; i < aliveTracklets.size(); i++) {
		dataFile << aliveTracklets[i].firstFrameDetected << " ";
		dataFile << aliveTracklets[i].lifetime << " ";
		dataFile << aliveTracklets[i].teamID << " ";
		for (int j = 0; j < aliveTracklets[i].positionHistory.size(); j++) {
			dataFile << aliveTracklets[i].positionHistory[j].x << " ";
			dataFile << aliveTracklets[i].positionHistory[j].y << " ";
		}
		dataFile << "\n";
	}

	// Dead tracklets
	for (int i = 0; i < deadTracklets.size(); i++) {
		dataFile << deadTracklets[i].firstFrameDetected << " ";
		dataFile << deadTracklets[i].lifetime << " ";
		dataFile << deadTracklets[i].teamID << " ";
		for (int j = 0; j < deadTracklets[i].positionHistory.size(); j++) {
			dataFile << deadTracklets[i].positionHistory[j].x << " ";
			dataFile << deadTracklets[i].positionHistory[j].y << " ";
		}
		dataFile << "\n";
	}
}