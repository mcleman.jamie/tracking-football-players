#include <iostream>
#include <opencv2/core/core.hpp>

#include "Application.hpp"


using namespace cv;

int main(int argc, char argv[]) {

	// Create list of camera names
	std::vector<std::string> cameraNames;
	cameraNames.push_back(std::string("1"));
	cameraNames.push_back(std::string("2"));
	cameraNames.push_back(std::string("3"));
	cameraNames.push_back(std::string("4"));
	cameraNames.push_back(std::string("5"));
	cameraNames.push_back(std::string("6"));

	// Create list of camera colours
	std::vector<Scalar> cameraColours;
	cameraColours.push_back(Scalar(0, 0, 255.0)); // Red
	cameraColours.push_back(Scalar(255.0, 0, 0)); // Blue
	cameraColours.push_back(Scalar(255.0, 255.0, 0)); // Cyan
	cameraColours.push_back(Scalar(0, 255.0, 255.0)); // Yellow
	cameraColours.push_back(Scalar(255.0, 0, 255.0)); // Pink
	cameraColours.push_back(Scalar(0, 0, 0)); // Black

	// Get users input for which application mode they want to execute
	printf("Enter 1 to see the aerial display\n");
	printf("Enter 2 to see player detections on camera image\n");
	printf("Enter 3 to see background subtractions\n");
	printf("Enter 4 to create and view pitch masks\n");
	printf("Enter 5 to create histograms\n");
	printf("Enter 6 to create backgrounds\n");
	printf("Enter 7 to display saved data\n");

	char input;

	while (true) {
		std::cin >> input;
		if (input == 49) { // 1
			ApplicationMain app = ApplicationMain(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 50) { // 2
			ApplicationViewDetections app = ApplicationViewDetections(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 51) { // 3
			ApplicationViewBackgroundSubtraction app = ApplicationViewBackgroundSubtraction(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 52) { // 4
			ApplicationCreatePitchMask app = ApplicationCreatePitchMask(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 53) { // 5
			ApplicationCreateHistograms app = ApplicationCreateHistograms(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 54) { // 6
			ApplicationCreateBackgrounds app = ApplicationCreateBackgrounds(cameraNames, cameraColours);
			app.start();
			break;
		}
		else if (input == 55) { // 7
			ApplicationDisplaySavedData app = ApplicationDisplaySavedData(cameraNames, cameraColours);
			app.start();
			break;
		}
		printf("Invalid input\n");
	}

	return 0;
}