#ifndef CAMERA_H
#define CAMERA_H
#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "Player.hpp"

using namespace cv;

class Camera {
public:
	Camera(std::string Name, Scalar Colour, int Id);
	bool nextFrame();
	bool readFrame(int FrameNum);
	void removeBackground();
	Point2f convertImageCoordToPitchCoord(Point2f ImageCoord);
	Point2f convertImageCoordToPitchCoord(Rect ImageCoord);
	void loadPitchMask();
private:
	void getHomography();
public:
	std::string name;
	int id;
	int framesPerSecond;
	Scalar colour;
	Mat homography;

	VideoCapture video;
	Mat currentFrame;
	Mat background;
	Mat foreground;
	Mat pitchMask;

	Mat closingElement;
	int thresholdValue;
};


#endif