#include "Camera.hpp"

Camera::Camera(std::string Name, Scalar Colour, int Id) {

	printf("Camera initialisation: %s\n", Name.c_str());

	// Initialise variables
	name = Name;
	colour = Colour;
	id = Id;
	thresholdValue = 15;

	// Kernel for the closing operation
	closingElement = getStructuringElement(MORPH_RECT, Size(9, 9));

	// Find homography
	getHomography();

	// Load video
	if (!video.open("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\" + name + ".avi")) {
		printf("ERROR opening video: %s\n", name.c_str());
	}

	// Get frames per second
	framesPerSecond = (int)video.get(CAP_PROP_FPS);

	// Load background
	background = imread("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\bg" + name + ".jpg", IMREAD_UNCHANGED);

}


bool Camera::nextFrame() {
	// Load the next frame in the video
	return video.read(currentFrame);
}

bool Camera::readFrame(int FrameNum) {

	// Loads a specific frame of the video
	if (FrameNum < 1) {
		return false;
	}

	// Set the frame
	video.set(CAP_PROP_POS_FRAMES, FrameNum);

	// Load the frame
	return video.read(currentFrame);
}

void Camera::removeBackground() {

	// Find the difference between the current frame and the background
	Mat difference;
	absdiff(currentFrame, background, difference);

	// Convert the difference image to 8 bit single channel
	cvtColor(difference, difference, COLOR_BGR2GRAY);

	// Threshold the difference image
	threshold(difference, foreground, thresholdValue, 255.0, THRESH_BINARY);

	// Closing operation applied to the foreground
	morphologyEx(foreground, foreground, MORPH_CLOSE, closingElement);

	// Gaussian blur applied to the foreground
	GaussianBlur(foreground, foreground, Size(5, 5), 5);

	// Bitwise and operation applied to the foreground and pitch mask
	bitwise_and(foreground, 0, foreground, pitchMask);
}

Point2f Camera::convertImageCoordToPitchCoord(Point2f ImageCoord) {

	Point2f pitchCoord;

	// Turn the image coordinate into a matrix
	Mat imageCoordsMat = (Mat_<double>(3, 1) << ImageCoord.x, ImageCoord.y, 1);

	// Transform using the homography
	Mat pitchCoordsMat = homography * imageCoordsMat;

	// Divide by the z value because the z value should be 1
	pitchCoordsMat /= pitchCoordsMat.at<double>(2, 0);

	// Add the coordinate to the vector
	pitchCoord.x = pitchCoordsMat.at<double>(0, 0);
	pitchCoord.y = pitchCoordsMat.at<double>(1, 0);
	return pitchCoord;
}

Point2f Camera::convertImageCoordToPitchCoord(Rect ImageCoord) {

	// Get the x and y values out of the Rect
	Point2f imagePos;
	imagePos.x = ImageCoord.x + ImageCoord.width / 2.0;
	imagePos.y = ImageCoord.y + ImageCoord.height;

	// Find the pitch coordinates
	return convertImageCoordToPitchCoord(imagePos);
}

void Camera::loadPitchMask() {

	// Load the pitch mask
	pitchMask = imread("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\pitchMask" + name + ".jpg", IMREAD_UNCHANGED);

	// Invert it so that the pitch is black and non-pitch is white
	bitwise_not(pitchMask, pitchMask);
}

void Camera::getHomography() {

	// Vectors which store the coordinates on the image and their corresponding coordinates in the pitch
	std::vector<Point2f> imagePoints;
	std::vector<Point2f> pitchPoints;

	// Open file for reading
	std::fstream dataFile("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\homographyData" + name + ".txt");

	float fileData[4]; // 4 float array for storing data in the file. 2 positions for each line

	// Loop through every line in the file
	while (dataFile >> fileData[0]) {

		dataFile >> fileData[1];
		dataFile >> fileData[2];
		dataFile >> fileData[3];

		// Append data to vectors
		imagePoints.push_back(Point2f(fileData[0], fileData[1]));
		pitchPoints.push_back(Point2f(fileData[2], fileData[3]));
	}

	dataFile.close(); // Close file

	// Find homography
	homography = findHomography(imagePoints, pitchPoints);
}
