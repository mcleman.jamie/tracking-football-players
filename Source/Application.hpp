#ifndef APPLICATION_H
#define APPLICATION_H
#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/video/tracking.hpp>

#include "Camera.hpp"
#include "TeamClassifier.hpp"
#include "Player.hpp"
#include "Tracking.hpp"
#include "PlayerDetection.hpp"
#include "Saving.hpp"

using namespace cv;

// Base class for general application structure
class Application {
public:
	Application(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours);
	void start() { return; }
private:
	void display() { return; }
protected:
	std::vector<Camera> cameras;
	int keyPressed;
};

// Main application that reads camera data, detects players, tracks them and stores them
class ApplicationMain : public Application {
public:
	ApplicationMain(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("Main application initialisation\n"); }
	void start();
private:
	bool update();
	void display();
	bool getInput();
	void saveFrame();

	std::vector<TrackedDetection> trackedDetections;
	std::vector<TrackedPlayer> tracklets;
	std::vector<TrackedPlayer> deadTracklets;

	Tracking playerTracking;
	TeamClassifier classifier;
	PlayerDetection detector;
	Saving saver;

	Mat pitchAerialGraphic;
	Mat pitchWithPlayers;

	bool nextFrame;
	int colourMode;
	bool viewDetections;
	bool viewTracklets;
	int currentFrame;

	int64 startTicks;
};

// Application that shows the detections of each camera
class ApplicationViewDetections : public Application {
public:
	ApplicationViewDetections(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("View detections application initialisation\n"); }
	void start();
private:
	void display();
	void saveFrame();
	std::vector<TrackedDetection> trackedDetections;
	TeamClassifier classifier;
	PlayerDetection detector;
	int currentCamera;
	bool nextFrame;
	int currentFrame;
	Mat frame;
};

// Application that shows the foreground for each camera
class ApplicationViewBackgroundSubtraction : public Application {
public:
	ApplicationViewBackgroundSubtraction(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("View background subtractions application initialisation\n"); }
	void start();
private:
	void display();
	void saveFrame();
	int currentCamera;
	bool nextFrame;
	bool isSubtraction;
	Mat frame;
};

// Application that creates a pitch mask for each camera
class ApplicationCreatePitchMask : public Application {
public:
	ApplicationCreatePitchMask(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("Create pitch mask application initialisation\n"); }
	void start();
private:
	void display();
	int currentCamera;
};

// Application that creates and stores histograms for different classe based on the users classifications
class ApplicationCreateHistograms : public Application {
public:
	ApplicationCreateHistograms(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("Create histograms application initialisation\n"); }
	void start();
private:
	void display();
	void trainPlayer(int teamID);
	void storeHistograms();
	Mat player;
	std::vector<TrackedDetection> trackedDetections;
	PlayerDetection detector;

	std::vector<std::string> teamNames;
	std::vector<std::vector<Mat>> histograms;
};

// Application that extracts backrounds via temporal median filtering
class ApplicationCreateBackgrounds : public Application {
public:
	ApplicationCreateBackgrounds(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("Creating backgrounds application initialisation\n"); }
	void start();
private:
	void display();
	void getMedian();
	Mat median;
	std::vector<Mat> frames;
};

// Application that displays or creates videos of saved tracklet and detection data
class ApplicationDisplaySavedData : public Application {
public:
	ApplicationDisplaySavedData(std::vector<std::string> cameraNames, std::vector<Scalar> cameraColours) : Application(cameraNames, cameraColours) { printf("Displaying saved data application initialisation\n"); }
	void start();
private:
	void display();
	bool loadPlayerTracklets();
	bool loadDetections(int Frame);

	std::string detectionsFilename;
	std::string trackletsFilename;
	std::fstream detectionsFile;
	Mat pitchAerialGraphic;
	int currentFrame;
	int currentCamera;

	std::vector<TrackedDetection> trackedDetections;
	std::vector<SavedPlayer> playerTracklets;

	VideoWriter aerialVideo;
	std::vector<VideoWriter> detectionVideos;

	bool paused;
	bool saveVideo;
};
#endif