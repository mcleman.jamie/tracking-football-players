#include "TeamClassifier.hpp"


bool TeamClassifier::loadHistograms(int NumBins) {

	numBins = NumBins;

	// Open the histograms file for reading
	std::fstream dataFile;
	dataFile.open("C:\\Users\\mclem\\Documents\\Uni\\YEAR 3 PROJECT\\Tracking Football Players\\Data\\Histograms.txt");

	int currentClassIndex = -1;
	float* b;
	float* g;
	float* r;
	std::string name = "";

	// Loop until the end of the file
	while (dataFile >> name) {

		printf("Team name: %s\n", name.c_str());

		currentClassIndex++;
		
		std::vector<Mat> newTeamHistogram;
		for (int i = 0; i < 3; i++) {
			newTeamHistogram.push_back(Mat::zeros(numBins, 1, CV_32FC1));
		}
		histograms.push_back(newTeamHistogram);
		
		// Iterate through the number of bins
		for (int i = 0; i < numBins; i++) {

			// Get pointers to the histogram data at bin i
			b = histograms[currentClassIndex][0].ptr<float>(i);
			g = histograms[currentClassIndex][1].ptr<float>(i);
			r = histograms[currentClassIndex][2].ptr<float>(i);

			// Read the data for the b, g and r values at bin i
			dataFile >> b[0];
			dataFile >> g[0];
			dataFile >> r[0];
		}
	}

	numClasses = currentClassIndex + 1;
	dataFile.close();

	// Find the total number of pixels classified for each class
	for (int i = 0; i < numClasses; i++) {
		histogramTotals.push_back(0);
		for (int j = 0; j < numBins; j++) {
			histogramTotals[i] += histograms[i][0].at<float>(j);
		}
	}

	// Normalise each histogram by dividing it by its total
	for (int i = 0; i < numClasses; i++) {
		for (int j = 0; j < numBins; j++) {
			histograms[i][0].at<float>(j, 0) /= histogramTotals[i];
			histograms[i][1].at<float>(j, 0) /= histogramTotals[i];
			histograms[i][2].at<float>(j, 0) /= histogramTotals[i];
		}
	}

	float smallest = 0;
	// Create noise floor histograms for each b, g, r, value
	noiseFloorHistograms.push_back(Mat::zeros(numBins, 1, CV_32FC1));
	noiseFloorHistograms.push_back(Mat::zeros(numBins, 1, CV_32FC1));
	noiseFloorHistograms.push_back(Mat::zeros(numBins, 1, CV_32FC1));

	// Iterate through each b, g, r value of each bin
	for (int i = 0; i < numBins; i++) {
		for (int x = 0; x < 3; x++) {

			// Find the smallest value out of all the classes for bin i and channel x
			smallest = min(histograms[0][x].at<float>(i, 0), histograms[1][x].at<float>(i, 0));
			smallest = min(smallest, histograms[2][x].at<float>(i, 0));

			// Set the noise floor to this value
			noiseFloorHistograms[x].at<float>(i, 0) = smallest;
		}
	}

	// Subtract each histogram by the noise floor value
	for (int i = 0; i < numClasses; i++) {
		for (int j = 0; j < numBins; j++) {
			histograms[i][0].at<float>(j, 0) -= noiseFloorHistograms[0].at<float>(j, 0);
			histograms[i][1].at<float>(j, 0) -= noiseFloorHistograms[1].at<float>(j, 0);
			histograms[i][2].at<float>(j, 0) -= noiseFloorHistograms[2].at<float>(j, 0);
		}
	}

	return true;
}

int TeamClassifier::classify(const Mat &ImageToClassify) {

	std::vector<float> percentages;
	return classify(ImageToClassify, percentages);
}

int TeamClassifier::classify(const Mat &ImageToClassify, std::vector<float> &Percentages) {

	// Clear percentages vector
	Percentages.clear();


	// Create empty histograms for this classification
	std::vector<Mat> bgrHists;
	for (int i = 0; i < 3; i++) {
		bgrHists.push_back(Mat::zeros(numBins, 1, CV_32FC1));
	}

	// Split the image into b, g, r channels and convert to floating point
	std::vector<Mat> bgrImages;
	split(ImageToClassify, bgrImages);
	bgrImages[0].convertTo(bgrImages[0], CV_32F);
	bgrImages[1].convertTo(bgrImages[1], CV_32F);
	bgrImages[2].convertTo(bgrImages[2], CV_32F);

	// Create a 2d Guassian kernel for weighting the centre pixels more than outside pixels
	Mat xKernel = getGaussianKernel(ImageToClassify.cols, ImageToClassify.cols / 10, CV_32F);
	Mat yKernel = getGaussianKernel(ImageToClassify.rows, ImageToClassify.rows / 10, CV_32F);
	Mat kernel = yKernel * xKernel.t();
	
	// Calculate histograms
	for (int i = 0; i < bgrImages.size(); i++) {
		for (int y = 0; y < bgrImages[i].rows; y++) {
			for (int x = 0; x < bgrImages[i].cols; x++) {
				bgrHists[i].at<float>((int)bgrImages[i].at<float>(y, x)) += kernel.at<float>(y, x);
			}
		}
	}

	// Calculate number of pixels of image
	int size = bgrImages[0].rows * bgrImages[0].cols;

	// Normalise the histograms by dividing by the number of pixels
	for (int i = 0; i < bgrHists[0].rows; i++) {
		bgrHists[0].at<float>(i, 0) /= size;
		bgrHists[1].at<float>(i, 0) /= size;
		bgrHists[2].at<float>(i, 0) /= size;
	}


	double bSimilarity, gSimilarity, rSimilarity, averageSimilarity;
	double mostSimilarValue = -1;
	int mostSimilarIndex = 0;
	double totalSimilarity = 0;

	// For each class
	for (int i = 0; i < numClasses; i++) {
		
		// Calculate similarities for each channel 
		bSimilarity = compareHist(bgrHists[0], histograms[i][0], 2);
		gSimilarity = compareHist(bgrHists[1], histograms[i][1], 2);
		rSimilarity = compareHist(bgrHists[2], histograms[i][2], 2);

		// Calculate the average similarity for this class
		averageSimilarity = (bSimilarity + gSimilarity + rSimilarity) / 3.0;

		// If this similarity is greater than the current most similar value
		if (averageSimilarity > mostSimilarValue) {

			mostSimilarIndex = i;
			mostSimilarValue = averageSimilarity;
		}
		// Append this similarity to the total value
		totalSimilarity += averageSimilarity;

		// Add this value to the percentages vector
		Percentages.push_back(averageSimilarity);
	}

	// Add values of 0 for the goalkeeper classes
	Percentages.push_back(0);
	Percentages.push_back(0);

	// For each percentage found, divide by the total similarity
	for (int i = 0; i < numClasses; i++) {
		float before = Percentages[i];
		Percentages[i] /= totalSimilarity;
	}

	// Return the most similar class
	return mostSimilarIndex;
}