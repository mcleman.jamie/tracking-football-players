#include "PlayerDetection.hpp"


void PlayerDetection::resetDetections() {
	
	// Clear detections
	detections.clear();
}

void PlayerDetection::getNewDetections(const Mat& Foreground, int CameraID) {

	std::vector<std::vector<Point>> contours;

	// Find contours method
	findContours(Foreground, contours, RETR_EXTERNAL, CHAIN_APPROX_TC89_KCOS);

	Point min, max;
	Rect newPlayerBox;
	Point2f topLeftPos;
	Point2f centrePos;
	Point2f size;

	// Iterate through all contours
	for (int i = 0; i < contours.size(); i++) {
		
		// If it's size is less than 5, ignore it
		if (contours[i].size() > 5) {

			min = contours[i][0];
			max = contours[i][0];

			// Iterate through all the points to find the minimum and maximum
			for (int j = 0; j < contours[i].size(); j++) {
				if (contours[i][j].x < min.x) {
					min.x = contours[i][j].x;
				}
				else if (contours[i][j].x > max.x) {
					max.x = contours[i][j].x;
				}
				if (contours[i][j].y < min.y) {
					min.y = contours[i][j].y;
				}
				else if (contours[i][j].y > max.y) {
					max.y = contours[i][j].y;
				}
			}

			// Size is the difference between min and max
			size.x = max.x - min.x;
			size.y = max.y - min.y;

			// If the size is too small, ignore it
			if (size.x > 10 && size.y > 50) {

				// Find the topLeft and centre positions
				topLeftPos.x = min.x;
				topLeftPos.y = min.y;
				centrePos.x = (min.x + max.x) / 2.0;
				centrePos.y = (min.y + max.y) / 2.0;

				// Create new detection
				Detection newDetection = Detection(topLeftPos, centrePos, size, CameraID);
				detections.push_back(newDetection);
			}
		}
	}
}

void PlayerDetection::update(std::vector<TrackedDetection> &trackedDetections) {
	
	// If there are no tracked detections, most likely first update
	if (trackedDetections.size() == 0) {

		// Track all detections
		for (int i = 0; i < detections.size(); i++) {
			trackedDetections.push_back(TrackedDetection(detections[i].boundingBox, detections[i].cameraDetectedWith));
		}
		return;
	}
	
	// Create a vector of bools the size of the number of trackedDetections
	// Set to true if the tracklet has been associated with a detection
	std::vector<bool> trackAssociated;
	for (int i = 0; i < trackedDetections.size(); i++) {
		trackAssociated.push_back(false);
	}

	// Predict the tracklets positions for this frame
	for (int i = 0; i < trackedDetections.size(); i++) {
		trackedDetections[i].predict();
	}

	Rect trackletPos;
	Rect detectionPos;

	// Associate detections to tracklets
	for (int i = 0; i < detections.size(); i++) {
		
		detectionPos = detections[i].boundingBox;

		for (int j = 0; j < trackedDetections.size(); j++) {

			if (detections[i].cameraDetectedWith == trackedDetections[j].cameraDetectedWith) {

				trackletPos = trackedDetections[j].predictedBoundingBox;

				// Calculates the area overlap percentage
				int xOverlap = max(0, min(detectionPos.x + detectionPos.width, trackletPos.x + trackletPos.width) - max(detectionPos.x, trackletPos.x));
				int yOverlap = max(0, min(detectionPos.y + detectionPos.height, trackletPos.y + trackletPos.height) - max(detectionPos.y, trackletPos.y));
				float overlapPercentage = (float)(xOverlap * yOverlap) / (float)(trackletPos.width * trackletPos.height);

				// If they overlap by 50% or more
				if (overlapPercentage >= 0.5) {
					// Associate detection i to tracklet j
					detections[i].associatedTracklets.push_back(j);
					trackAssociated[j] = true;
				}
			}
		}
	}


	// Correct the tracklets based on what detections they are associated with
	for (int i = 0; i < detections.size(); i++) {

		int numberOfAssociations = detections[i].associatedTracklets.size();

		// No associations so create a new tracklet
		if (numberOfAssociations == 0) {
			trackedDetections.push_back(TrackedDetection(detections[i].boundingBox, detections[i].cameraDetectedWith));
		}

		// 1 association so correct with this detection
		else if (numberOfAssociations == 1) {
			trackedDetections[detections[i].associatedTracklets[0]].correct(detections[i].boundingBox, false);
		}

		// Multiple associations so estimate bounding boxes
		else {

			std::vector<int> associations = detections[i].getAssociatedTracklets();
			std::vector<Rect> predictedBoundingBoxes;
			Rect detectedBoundingBox = detections[i].boundingBox;
			int leftID = 0;
			int rightID = 0;
			int topID = 0;
			int bottomID = 0;

			// For every associated tracked detections, add its predicted boundingBox to estimatedBoundingBox
			for (int j = 0; j < associations.size(); j++) {
				predictedBoundingBoxes.push_back(trackedDetections[associations[j]].predictedBoundingBox);

				// If the bounding box is greater than the detected box, make it smaller to fit
				if (predictedBoundingBoxes.back().width > detectedBoundingBox.width) {
					predictedBoundingBoxes.back().width = detectedBoundingBox.width;
					predictedBoundingBoxes.back().x = detectedBoundingBox.x;
				}
				if (predictedBoundingBoxes.back().height > detectedBoundingBox.height) {
					predictedBoundingBoxes.back().height = detectedBoundingBox.height;
					predictedBoundingBoxes.back().y = detectedBoundingBox.y;
				}
			}

			// For every predicted bounding box, find the furthest left, right, top and bottom
			for (int j = 1; j < predictedBoundingBoxes.size(); j++) {

				if (predictedBoundingBoxes[j].x < predictedBoundingBoxes[leftID].x) {
					leftID = j;
				}
				if (predictedBoundingBoxes[j].x + predictedBoundingBoxes[j].width > predictedBoundingBoxes[rightID].x + predictedBoundingBoxes[rightID].width) {
					rightID = j;
				}
				if (predictedBoundingBoxes[j].y < predictedBoundingBoxes[topID].y) {
					topID = j;
				}
				if (predictedBoundingBoxes[j].y + predictedBoundingBoxes[j].height > predictedBoundingBoxes[bottomID].y + predictedBoundingBoxes[bottomID].height) {
					bottomID = j;
				}
			}

			// Set the furthest left/right/top/bottom predicted boxes to the corresponding edge of the detected box
			predictedBoundingBoxes[leftID].x = detectedBoundingBox.x;
			predictedBoundingBoxes[rightID].x = detectedBoundingBox.x + detectedBoundingBox.width - predictedBoundingBoxes[rightID].width;
			predictedBoundingBoxes[topID].y = detectedBoundingBox.y;
			predictedBoundingBoxes[bottomID].y = detectedBoundingBox.y + detectedBoundingBox.height - predictedBoundingBoxes[bottomID].height;

			// Correct each associated tracklet with its new predicted bounding box
			for (int j = 0; j < associations.size(); j++) {
				trackedDetections[associations[j]].correct(predictedBoundingBoxes[j], true);
			}
		}
	}
	// Remove any undetected tracklets
	for (int i = trackAssociated.size() - 1; i >= 0; i--) {
		if (trackAssociated[i] == false) {
			trackedDetections.erase(trackedDetections.begin() + i);
		}
	}
	
}

void PlayerDetection::validateTrackedDetections(std::vector<TrackedDetection>& trackedDetections, int MaxX, int MaxY) {

	// Check that every detection is within the frame

	Rect boundingBox;

	for (int i = 0; i < trackedDetections.size(); i++) {

		boundingBox = trackedDetections[i].boundingBox;

		if (boundingBox.x < 0) {
			trackedDetections.erase(trackedDetections.begin() + i);
			i--;
		}
		else if (boundingBox.x + boundingBox.width > MaxX) {
			trackedDetections.erase(trackedDetections.begin() + i);
			i--;
		}
		if (boundingBox.y < 0) {
			trackedDetections.erase(trackedDetections.begin() + i);
			i--;
		}
		else if (boundingBox.y + boundingBox.height > MaxY) {
			trackedDetections.erase(trackedDetections.begin() + i);
			i--;
		}
	}
}