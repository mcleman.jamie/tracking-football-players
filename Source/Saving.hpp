#ifndef SAVING_H
#define SAVING_H
#include <iostream>
#include <vector>
#include <fstream>
#include "Player.hpp"


class Saving {
public:
	Saving();
	void init(std::string DetectionsFileName, std::string TrackletsFileName);
	void saveDetections(int FrameNumber, const std::vector<TrackedDetection> Detections);
	void saveTracklets(const std::vector<TrackedPlayer> aliveTracklets, const std::vector<TrackedPlayer> deadTracklets);
	std::string detectionsFileName;
	std::string trackletsFileName;
};

#endif