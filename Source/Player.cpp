#include "Player.hpp"

Detection::Detection(Point2f TopLeftPos, Point2f CentrePos, Point2f Size, int CameraDetectedWith) {

	// Initialise detection values
	topLeftPos = TopLeftPos;
	centrePos = CentrePos;
	size = Size;
	boundingBox = Rect(topLeftPos.x, topLeftPos.y, size.x, size.y);
	cameraDetectedWith = CameraDetectedWith;
}

TrackedDetection::TrackedDetection(Rect BoundingBox, int CameraDetectedWith) {
	
	// Initialise tracked detection values
	boundingBox = BoundingBox;
	predictedBoundingBox.x = BoundingBox.x;
	predictedBoundingBox.y = BoundingBox.y;
	cameraDetectedWith = CameraDetectedWith;

	centrePos.x = boundingBox.x + boundingBox.width / 2;
	centrePos.y = boundingBox.y + boundingBox.height / 2;
	boundingBoxVelocity = Rect(0, 0, 0, 0);
	occluded = false;
}

void TrackedDetection::predict() {

	// predicted position = current position + velocity
	predictedBoundingBox.x = boundingBox.x + boundingBoxVelocity.x;
	predictedBoundingBox.y = boundingBox.y + boundingBoxVelocity.y;

	// Size of box predicted to be constant
	predictedBoundingBox.width = boundingBox.width;
	predictedBoundingBox.height = boundingBox.height;

}

void TrackedDetection::correct(Rect CorrectedBoundingBox, bool Occluded) {

	// Get the centre of the corrected bounding box
	Point2f correctedCentre;
	correctedCentre.x = CorrectedBoundingBox.x + CorrectedBoundingBox.width / 2;
	correctedCentre.y = CorrectedBoundingBox.y + CorrectedBoundingBox.height / 2;

	// Calculate velocity based on average of last 3 frames
	boundingBoxVelocity.x = ((boundingBoxVelocity.x * 2) + correctedCentre.x - centrePos.x) / 3;
	boundingBoxVelocity.y = ((boundingBoxVelocity.y * 2) + correctedCentre.y - centrePos.y) / 3;

	// Update centre position and bounding box
	centrePos = correctedCentre;
	boundingBox = CorrectedBoundingBox;

	// Set occluded flag
	occluded = Occluded;
}

// Static int in TrackedPlayer to store the nextID available
int TrackedPlayer::nextID = 0;

TrackedPlayer::TrackedPlayer() {
	
	// Initialise values
	ID = -1;
	velocity = Point2f(0, 0);
	updatesWithoutDetection = 0;
	lifetime = 1;
	previousSmoothedTeamValue = -1;
	alive = true;
	teamSwitched = false;
	switchedToID = -1;
	possibleSwitch = false;
}

void TrackedPlayer::init(Point2f Position, std::vector<float> TeamPercentages, int FirstFrameDetected) {

	// If the ID hasn't been set yet
	if (ID == -1) {

		// Set the ID to the next available and increment next available
		ID = nextID;
		nextID++;
	}

	// Set values with parameters
	position = Position;
	predictedPosition = Position;
	teamPercentages = TeamPercentages;
	firstFrameDetected = FirstFrameDetected;

	// Find the team ID by finding the max percentage
	float maxPercentage = teamPercentages[0];
	int maxTeamID = 0;
	for (int i = 1; i < teamPercentages.size(); i++) {
		if (teamPercentages[i] > maxPercentage) {
			maxPercentage = teamPercentages[i];
			maxTeamID = i;
		}
	}
	teamID = maxTeamID;

	// Initialise the team classification frequency vector
	teamClassificationFrequency.clear();
	for (int i = 0; i < teamPercentages.size(); i++) {
		teamClassificationFrequency.push_back(0);
	}
	// Increment for the team ID found
	teamClassificationFrequency[teamID]++;

	// Add first values for position and team history vectors
	positionHistory.clear();
	positionHistory.push_back(position);
	teamHistory.clear();
	teamHistory.push_back(teamID);

}

void TrackedPlayer::predict() {

	// Predict the position of the tracklet next frame
	predictedPosition = position + velocity;
}

void TrackedPlayer::correctPosition(Point2f CorrectedPosition) {

	// Increment lifetime
	lifetime++;

	 // Append previous position to history
	positionHistory.push_back(position);

	// Calculate current velocity based on average of previous 3 velocities
	velocity = ((velocity * 2) + CorrectedPosition - position) / 3;

	// Set position
	position = CorrectedPosition;

	// Reset number of updates without detection
	updatesWithoutDetection = 0;
}

void TrackedPlayer::correctTeam(std::vector<float>& TeamPercentages) {

	// This tracklet is occluded in all views
	if (TeamPercentages[0] == -1) {

		// This means there is a possible switch of teams
		possibleSwitch = true;
		switchCountdown = smoothingKernelSize;
		return;
	}

	// Set team percentages
	teamPercentages = TeamPercentages;

	// Find the team ID of this frame
	float maxPercentage = TeamPercentages[0];
	int maxTeamID = 0;

	for (int i = 1; i < TeamPercentages.size(); i++) {
		if (TeamPercentages[i] > maxPercentage) {
			maxPercentage = TeamPercentages[i];
			maxTeamID = i;
		}
	}

	// Increment the team classification frequency
	teamClassificationFrequency[maxTeamID]++;

	// If this team classification has a greater frequency than the current team classification
	if (teamClassificationFrequency[maxTeamID] > teamClassificationFrequency[teamID]) {
		// Set the new team ID
		teamID = maxTeamID;
	}

	// Append to the team history vector
	teamHistory.push_back(maxTeamID);

	// If there has been a possible switch
	if (possibleSwitch) {
		if (teamHistory.size() >= smoothingKernelSize) {

			// Smooths the previous x classifications by finding the mode

			// Find the frequency of each classification in the previous x classification where x = smoothingKernelSize
			std::vector<int> frequencyHist(5, 0);
			for (int i = teamHistory.size() - smoothingKernelSize; i < teamHistory.size(); i++) {
				frequencyHist[teamHistory[i]]++;
			}

			// Find the mode classification
			int mode = std::max_element(frequencyHist.begin(), frequencyHist.end()) - frequencyHist.begin();

			// If we haven't previously set the previous smoothed team value
			if (previousSmoothedTeamValue == -1) {
				previousSmoothedTeamValue = mode;
			}
			else {

				// By doing [-1, 1] of the previous smoothed value and current we can determine a change
				int edgeValue = -1 * previousSmoothedTeamValue + mode;

				// If edgeValue is not 0, we have switched team
				if (edgeValue != 0) {
					teamSwitched = true;
				}

				previousSmoothedTeamValue = mode;
			}

			// Decrement the switch countdown until it reached 0
			switchCountdown--;
			if (switchCountdown <= 0) {
				previousSmoothedTeamValue = -1;
				possibleSwitch = false;
			}
		}
	}
}

void TrackedPlayer::correctUndetected() {

	// Update lifetime
	lifetime++;

	// Append position history
	positionHistory.push_back(position);

	// Set new position to the predicted position
	position = predictedPosition;

	// If lifetime is less than 5, the tracklet is dead
	if (lifetime < 5) {
		alive = false;
	}

	// If the number of frames without detection is greater than 5, the tracklet is dead
	updatesWithoutDetection++;
	if (updatesWithoutDetection >= 5) {

		// Erase the previous 5 positions as these have all been predicted, not detected
		for (int i = 0; i < 5; i++) {
			positionHistory.erase(positionHistory.end() - 1);
			lifetime--;
		}
		alive = false;
	}
}

void TrackedPlayer::switchToNewTracklet(TrackedPlayer& newTracklet, int CurrentFrame) {

	// Initialise a new tracklet with the values of this tracklet
	newTracklet.init(this->position, this->teamPercentages, CurrentFrame);

	// Erase the last position as the new tracklet has it now
	this->positionHistory.erase(this->positionHistory.end() - 1);
	lifetime--;

	// Set the ID which this tracklet has switched to
	this->switchedToID = newTracklet.ID;
}

void TrackedPlayer::mergeTracklet(TrackedPlayer& TrackletToMerge) {

	// Merge TrackletToMerge to the end of this tracklet

	// Append the positions of the merging tracklet to the end of the this tracklet
	for (int i = 0; i < TrackletToMerge.positionHistory.size(); i++) {
		this->positionHistory.push_back(TrackletToMerge.positionHistory[i]);
	}

	// Add the lifetimes together
	this->lifetime += TrackletToMerge.lifetime;

	// Set end values of this tracklet to end values of the merging trackelt
	this->alive = TrackletToMerge.alive;
	this->teamSwitched = TrackletToMerge.teamSwitched;
	this->switchedToID = TrackletToMerge.switchedToID;
}