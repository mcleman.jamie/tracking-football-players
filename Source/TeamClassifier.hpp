#ifndef TEAM_CLASSIFIER_H
#define TEAM_CLASSIFIER_H
#include <iostream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;

class TeamClassifier {
public:
	TeamClassifier() { return; }
	bool loadHistograms(int NumBins = 256);
	int classify(const Mat& ImageToClassify);
	int classify(const Mat& ImageToClassify, std::vector<float> &Percentages);
private:
	int numBins;
	int numClasses;
	std::vector<std::vector<Mat>> histograms;
	std::vector<float> histogramTotals;
	std::vector<Mat> noiseFloorHistograms;
};


#endif