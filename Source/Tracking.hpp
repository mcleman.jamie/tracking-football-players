#ifndef TRACKING_H
#define TRACKING_H
#include <opencv2/core/core.hpp>
#include "Player.hpp"

using namespace cv;

class Tracking {
public:
	Tracking();
	void update(int CurrentFrame, std::vector<TrackedPlayer> &Tracklets, const std::vector<TrackedDetection> &Detections);
private:

	void initialiseTracklets(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);
	void associateDetectionsToTracklets(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);

	void combineDetections(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);
	bool getAveragesOfDetections(int TrackletID, const std::vector<TrackedDetection>& Detections, Point2f &AveragePosition, std::vector<float> &AverageTeamPercentages);
	void findExtremeTracklets(const std::vector<TrackedPlayer> Tracklets, int& Left, int& Right, int& Top, int& Bottom);

	bool applyConstraints(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);
	bool mergeConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);
	void sameCameraConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);
	void singleDetectionConstraint(std::vector<TrackedPlayer>& Tracklets, const std::vector<TrackedDetection>& Detections);

	void checkIfSingleAssociation(int DetectionID);
	int getClosestDetection(const std::vector<TrackedDetection>& Detections, Point2f Point, std::vector<int>& DetectionsToCheck);

	void addColumnToMat(Mat& m);
	void printAssociationMatrix();
	bool compareMats(const Mat& A, const Mat& B);

private:
	bool startedTracking;
	float distanceAssociationThreshold;
	Mat associationMatrix;
	int currentFrame;
};

#endif